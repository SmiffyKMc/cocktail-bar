import 'package:cocktail_application/core/util/string_manipulator.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockStringManipulator extends Mock implements StringManipulator {
  String removeNumbersFromString(String stringToClean) {
    final stringToBeReturned = stringToClean.replaceAll(RegExp(r"[0-9]"), '');
    return stringToBeReturned;
  }

  List<String> splitStringByDot(String stringToSplit) {
    final listOfSplittedStrings =
        stringToSplit.split('.').map((sentence) => sentence.trim()).toList();
    final listWithClearedWhitespace = <String>[];
    listOfSplittedStrings.forEach((sentence) {
      if (sentence.length > 0) listWithClearedWhitespace.add('$sentence.');
    });
    return listWithClearedWhitespace;
  }

  List<String> capitalizeFirstWord(List<String> stringToSplit) {
    return stringToSplit
        .map((sentence) => sentence[0].toUpperCase() + sentence.substring(1))
        .toList();
  }

  @override
  List<String> formatInstructions(String stringToFormat) {
    final formatedString = removeNumbersFromString(stringToFormat);
    final stringSplitToList = splitStringByDot(formatedString);
    return capitalizeFirstWord(stringSplitToList);
  }
}

void main() {
  MockStringManipulator mockStringManipulator;
  setUp(() {
    mockStringManipulator = MockStringManipulator();
  });
  final tInstructions =
      "1. Fill a rocks glass with ice 2.add white creme de cacao and vodka 3.stir";

  test('should remove any number from the sentance.', () async {
    final tExpectedInstructions =
        ". Fill a rocks glass with ice .add white creme de cacao and vodka .stir";
    final manipulatedString =
        mockStringManipulator.removeNumbersFromString(tInstructions);
    //assert
    expect(manipulatedString, equals(tExpectedInstructions));
  });

  test(
      'should split the string by the dot and fix dot placement while trimming spaces',
      () async {
    //arrange
    final tStringToManipulate =
        ". Fill a rocks glass with ice .add white creme de cacao and vodka .stir";
    final tExpectedInstructions = [
      'Fill a rocks glass with ice.',
      'add white creme de cacao and vodka.',
      'stir.'
    ];

    //act
    final manipulatedString =
        mockStringManipulator.splitStringByDot(tStringToManipulate);
    //assert
    expect(manipulatedString, equals(tExpectedInstructions));
  });

  test('should capitalize the first letter of a work in the sentence',
      () async {
    //arrange
    final tInstructionsToManipulate = [
      'Fill a rocks glass with ice.',
      'add white creme de cacao and vodka.',
      'stir.'
    ];
    final tExpectedInstructions = [
      'Fill a rocks glass with ice.',
      'Add white creme de cacao and vodka.',
      'Stir.'
    ];
    //act
    final manipulatedString =
        mockStringManipulator.capitalizeFirstWord(tInstructionsToManipulate);
    //assert
    expect(manipulatedString, equals(tExpectedInstructions));
  });

  test('should fix string of instructions to list of instructions formatted',
      () async {
    //arrange
    final tInstructionsToFormat =
        "1. Fill a rocks glass with ice 2.add white creme de cacao and vodka 3.stir";
    final tExpectedInstructions = [
      'Fill a rocks glass with ice.',
      'Add white creme de cacao and vodka.',
      'Stir.'
    ];
    //act
    final manipulatedString =
        mockStringManipulator.formatInstructions(tInstructionsToFormat);
    //assert
    expect(manipulatedString, equals(tExpectedInstructions));
  });
}
