import 'package:cocktail_application/core/usecases/usecase.dart';
import 'package:cocktail_application/domain/entities/cocktail_details.dart';
import 'package:cocktail_application/domain/repositories/cocktail_repository.dart';
import 'package:cocktail_application/domain/usecases/get_cocktails_from_api/get_random_cocktail.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockCocktailRepository extends Mock implements CocktailReposity {}

void main() {
  GetRandomCocktail usecase;
  MockCocktailRepository mockCocktailRepository;

  setUp(() {
    mockCocktailRepository = MockCocktailRepository();
    usecase = GetRandomCocktail(mockCocktailRepository);
  });

  final tCocktail = CocktailDetails(
      idDrink: '1',
      strDrink: 'Test Cocktail',
      strTags: 'Classic,IBA',
      strCategory: 'Ordinary Drink',
      strIBA: 'Unforgettables',
      strAlcoholic: 'Alcholic',
      strGlass: 'Cocktail Glass',
      strInstructions: 'Stir into glass over ice, garnish and serve.',
      strDrinkThumb:
          'https://www.thecocktaildb.com/images/media/drink/qgdu971561574065.jpg',
      strIngredients: [
        'gin',
        'campari',
        'sweet vermouth'
      ],
      strMeasures: [
        '1 oz',
        '1 oz',
        '1 oz',
      ]);

  test('should get a random Cocktail Details from the cocktail repository',
      () async {
    //arrange
    when(mockCocktailRepository.getRandomCocktail())
        .thenAnswer((realInvocation) async => right(tCocktail));
    //act
    final result = await usecase(NoParams());
    //assert

    expect(result, right(tCocktail));
    verify(mockCocktailRepository.getRandomCocktail());
  });
}
