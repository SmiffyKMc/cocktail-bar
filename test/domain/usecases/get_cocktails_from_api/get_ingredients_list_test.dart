import 'package:cocktail_application/core/usecases/usecase.dart';
import 'package:cocktail_application/domain/entities/ingredient.dart';
import 'package:cocktail_application/domain/repositories/cocktail_repository.dart';
import 'package:cocktail_application/domain/usecases/get_cocktails_from_api/get_ingredients_list.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockCocktailRepository extends Mock implements CocktailReposity {}

void main() {
  GetIngredientsList usecase;
  MockCocktailRepository mockCocktailRepository;

  setUp(() {
    mockCocktailRepository = MockCocktailRepository();
    usecase = GetIngredientsList(mockCocktailRepository);
  });

  final tIngredientsList = [Ingredient(strIngredient1: 'Gin')];

  test('should get a list of Ingredients from the cocktail repository',
      () async {
    //arrange
    when(mockCocktailRepository.getIngredientsList())
        .thenAnswer((realInvocation) async => right(tIngredientsList));

    //act
    final result = await usecase(NoParams());

    //assert
    expect(result, right(tIngredientsList));
    verify(mockCocktailRepository.getIngredientsList());
  });
}
