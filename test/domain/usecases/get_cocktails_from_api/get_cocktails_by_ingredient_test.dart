import 'package:cocktail_application/domain/entities/cocktail.dart';
import 'package:cocktail_application/domain/repositories/cocktail_repository.dart';
import 'package:cocktail_application/domain/usecases/get_cocktails_from_api/get_cocktails_by_ingredient.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockCocktailRepository extends Mock implements CocktailReposity {}

void main() {
  GetCocktailsByIngredient usecase;
  MockCocktailRepository mockCocktailRepository;

  setUp(() {
    mockCocktailRepository = MockCocktailRepository();
    usecase = GetCocktailsByIngredient(mockCocktailRepository);
  });

  final tIngredient = 'Test Category';
  final tCocktailsList = [
    Cocktail(
      idDrink: '1',
      strDrink: 'Test Cocktail',
      strDrinkThumb:
          'https://www.thecocktaildb.com/images/media/drink/qgdu971561574065.jpg',
    )
  ];

  test(
      'should get a List of Cocktails from the cocktail repository when given an Ingredient',
      () async {
    //arrange
    when(mockCocktailRepository.getCocktailsByIngredient(tIngredient))
        .thenAnswer((realInvocation) async => right(tCocktailsList));

    //act
    final result = await usecase(Params(ingredient: tIngredient));

    //assert
    expect(result, right(tCocktailsList));
    verify(mockCocktailRepository.getCocktailsByIngredient(tIngredient));
  });
}
