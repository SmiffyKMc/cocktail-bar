import 'package:cocktail_application/domain/entities/cocktail.dart';
import 'package:cocktail_application/domain/repositories/cocktail_repository.dart';
import 'package:cocktail_application/domain/usecases/get_cocktails_from_api/get_cocktails_by_glass.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockCocktailRepository extends Mock implements CocktailReposity {}

void main() {
  GetCocktailsByGlass usecase;
  MockCocktailRepository mockCocktailRepository;

  setUp(() {
    mockCocktailRepository = MockCocktailRepository();
    usecase = GetCocktailsByGlass(mockCocktailRepository);
  });

  final tGlass = 'Test Category';
  final tCocktailsList = [
    Cocktail(
        idDrink: '1',
        strDrink: 'Test Cocktail',
        strDrinkThumb:
            'https://www.thecocktaildb.com/images/media/drink/qgdu971561574065.jpg')
  ];

  test('should get a List of Cocktails when given a Glass type.', () async {
    //arrange
    when(mockCocktailRepository.getCocktailsByGlass(any))
        .thenAnswer((realInvocation) async => right(tCocktailsList));

    //act
    final result = await usecase(Params(glass: tGlass));

    //assert
    expect(result, right(tCocktailsList));
    verify(mockCocktailRepository.getCocktailsByGlass(tGlass));
  });
}
