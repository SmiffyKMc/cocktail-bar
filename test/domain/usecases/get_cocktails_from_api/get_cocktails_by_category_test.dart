import 'package:cocktail_application/domain/entities/cocktail.dart';
import 'package:cocktail_application/domain/repositories/cocktail_repository.dart';
import 'package:cocktail_application/domain/usecases/get_cocktails_from_api/get_cocktails_by_category.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockCocktailRepository extends Mock implements CocktailReposity {}

void main() {
  GetCocktailsByCategory usecase;
  MockCocktailRepository mockCocktailRepository;

  setUp(() {
    mockCocktailRepository = MockCocktailRepository();
    usecase = GetCocktailsByCategory(mockCocktailRepository);
  });

  final tCategory = 'Test Category';
  final tCocktail = [
    Cocktail(
        idDrink: '1',
        strDrink: 'Test Cocktail',
        strDrinkThumb:
            'https://www.thecocktaildb.com/images/media/drink/qgdu971561574065.jpg')
  ];

  test(
      'should get a cocktail from the cocktail repository when given a Category',
      () async {
    //arrange
    when(mockCocktailRepository.getCocktailsByCategory(any))
        .thenAnswer((realInvocation) async => right(tCocktail));

    //act
    final result = await usecase(Params(category: tCategory));

    //assert
    expect(result, right(tCocktail));
    verify(mockCocktailRepository.getCocktailsByCategory(tCategory));
  });
}
