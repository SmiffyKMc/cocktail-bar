import 'package:cocktail_application/domain/entities/cocktail_details.dart';
import 'package:cocktail_application/domain/repositories/favourites_repository.dart';
import 'package:cocktail_application/domain/usecases/get_favourite_cocktails/add_cocktail_to_favourites.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockFavouritesRepository extends Mock implements FavouritesRepository {}

void main() {
  MockFavouritesRepository mockFavouritesRepository;
  AddCocktailToFavourites addCocktailToFavourites;

  setUp(() {
    mockFavouritesRepository = MockFavouritesRepository();
    addCocktailToFavourites = AddCocktailToFavourites(mockFavouritesRepository);
  });

  final tFavouriteCocktail = CocktailDetails(
      idDrink: '1',
      strDrink: 'Test Cocktail',
      strTags: 'Classic,IBA',
      strCategory: 'Ordinary Drink',
      strIBA: 'Unforgettables',
      strAlcoholic: 'Alcholic',
      strGlass: 'Cocktail Glass',
      strInstructions: 'Stir into glass over ice, garnish and serve.',
      strDrinkThumb:
          'https://www.thecocktaildb.com/images/media/drink/qgdu971561574065.jpg',
      strIngredients: [
        'gin',
        'campari',
        'sweet vermouth'
      ],
      strMeasures: [
        '1 oz',
        '1 oz',
        '1 oz',
      ]);

  test('should return true if cocktail was added to favourites successfully',
      () async {
    //arrange
    when(mockFavouritesRepository.addCocktailToFavourites(any))
        .thenAnswer((realInvocation) async => right(1));
    //act
    final result = await addCocktailToFavourites
        .call(Params(cocktailDetails: tFavouriteCocktail));
    //assert
    expect(result, right(1));
    verify(mockFavouritesRepository.addCocktailToFavourites(any));
  });
}
