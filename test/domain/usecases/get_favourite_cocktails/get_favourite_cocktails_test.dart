import 'package:cocktail_application/core/usecases/usecase.dart';
import 'package:cocktail_application/domain/entities/cocktail_details.dart';
import 'package:cocktail_application/domain/repositories/favourites_repository.dart';
import 'package:cocktail_application/domain/usecases/get_favourite_cocktails/get_favourite_cocktails.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockFavouritesRepository extends Mock implements FavouritesRepository {}

void main() {
  GetFavouriteCocktails getFavouriteCocktails;
  MockFavouritesRepository mockFavouritesRepository;
  setUp(() {
    mockFavouritesRepository = MockFavouritesRepository();
    getFavouriteCocktails = GetFavouriteCocktails(mockFavouritesRepository);
  });
  final tFavouriteCocktails = [
    CocktailDetails(
        idDrink: '1',
        strDrink: 'Test Cocktail',
        strTags: 'Classic,IBA',
        strCategory: 'Ordinary Drink',
        strIBA: 'Unforgettables',
        strAlcoholic: 'Alcholic',
        strGlass: 'Cocktail Glass',
        strInstructions: 'Stir into glass over ice, garnish and serve.',
        strDrinkThumb:
            'https://www.thecocktaildb.com/images/media/drink/qgdu971561574065.jpg',
        strIngredients: [
          'gin',
          'campari',
          'sweet vermouth'
        ],
        strMeasures: [
          '1 oz',
          '1 oz',
          '1 oz',
        ])
  ];

  test('should return a list of favourite cocktails', () async {
    //arrange
    when(mockFavouritesRepository.getFavouriteCocktails())
        .thenAnswer((_) async => right(tFavouriteCocktails));
    //act
    final result = await getFavouriteCocktails.call(NoParams());
    //assert
    expect(result, right(tFavouriteCocktails));
    verify(mockFavouritesRepository.getFavouriteCocktails());
  });
}
