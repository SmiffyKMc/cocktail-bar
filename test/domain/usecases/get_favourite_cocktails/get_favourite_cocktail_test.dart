import 'package:cocktail_application/domain/entities/cocktail_details.dart';
import 'package:cocktail_application/domain/repositories/favourites_repository.dart';
import 'package:cocktail_application/domain/usecases/get_favourite_cocktails/get_favourtie_cocktail.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockFavouritesRespository extends Mock implements FavouritesRepository {}

void main() {
  MockFavouritesRespository mockFavouritesRespository;
  GetFavouriteCocktail getFavouriteCocktail;

  setUp(() {
    mockFavouritesRespository = MockFavouritesRespository();
    getFavouriteCocktail = GetFavouriteCocktail(mockFavouritesRespository);
  });
  final tIdDrink = '1';
  final tFavouriteCocktail = CocktailDetails(
      idDrink: '1',
      strDrink: 'Test Cocktail',
      strTags: 'Classic,IBA',
      strCategory: 'Ordinary Drink',
      strIBA: 'Unforgettables',
      strAlcoholic: 'Alcholic',
      strGlass: 'Cocktail Glass',
      strInstructions: 'Stir into glass over ice, garnish and serve.',
      strDrinkThumb:
          'https://www.thecocktaildb.com/images/media/drink/qgdu971561574065.jpg',
      strIngredients: [
        'gin',
        'campari',
        'sweet vermouth'
      ],
      strMeasures: [
        '1 oz',
        '1 oz',
        '1 oz',
      ]);

  test('should return a favourite cocktail', () async {
    //arrange
    when(mockFavouritesRespository.getFavouriteCocktail(any))
        .thenAnswer((realInvocation) async => right(tFavouriteCocktail));
    //act
    final result =
        await getFavouriteCocktail.call(Params(cocktailId: tIdDrink));
    //assert
    expect(result, right(tFavouriteCocktail));
    verify(mockFavouritesRespository.getFavouriteCocktail(tIdDrink));
  });
}
