import 'package:cocktail_application/domain/entities/cocktail_details.dart';
import 'package:cocktail_application/domain/repositories/favourites_repository.dart';
import 'package:cocktail_application/domain/usecases/get_favourite_cocktails/remove_cocktail_from_favourites.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockFavouritesRepository extends Mock implements FavouritesRepository {}

void main() {
  MockFavouritesRepository mockFavouritesRepository;
  RemoveCocktailFromFavourites removeCocktailFromFavourites;

  setUp(() {
    mockFavouritesRepository = MockFavouritesRepository();
    removeCocktailFromFavourites =
        RemoveCocktailFromFavourites(mockFavouritesRepository);
  });

  final tIdDrink = '1';
  final tFavouriteCocktails = [
    CocktailDetails(
        idDrink: '1',
        strDrink: 'Test Cocktail',
        strTags: 'Classic,IBA',
        strCategory: 'Ordinary Drink',
        strIBA: 'Unforgettables',
        strAlcoholic: 'Alcholic',
        strGlass: 'Cocktail Glass',
        strInstructions: 'Stir into glass over ice, garnish and serve.',
        strDrinkThumb:
            'https://www.thecocktaildb.com/images/media/drink/qgdu971561574065.jpg',
        strIngredients: [
          'gin',
          'campari',
          'sweet vermouth'
        ],
        strMeasures: [
          '1 oz',
          '1 oz',
          '1 oz',
        ]),
    CocktailDetails(
        idDrink: '2',
        strDrink: 'Test Cocktail',
        strTags: 'Classic,IBA',
        strCategory: 'Ordinary Drink',
        strIBA: 'Unforgettables',
        strAlcoholic: 'Alcholic',
        strGlass: 'Cocktail Glass',
        strInstructions: 'Stir into glass over ice, garnish and serve.',
        strDrinkThumb:
            'https://www.thecocktaildb.com/images/media/drink/qgdu971561574065.jpg',
        strIngredients: [
          'gin',
          'campari',
          'sweet vermouth'
        ],
        strMeasures: [
          '1 oz',
          '1 oz',
          '1 oz',
        ])
  ];

  test('should return true when cocktail was removed from favourites',
      () async {
    //arrange
    when(mockFavouritesRepository.removeCocktailFromFavourites(any))
        .thenAnswer((realInvocation) async => right(1));
    //act
    final result =
        await removeCocktailFromFavourites.call(Params(cocktailId: tIdDrink));
    //assert
    expect(result, right(1));
    verify(mockFavouritesRepository.removeCocktailFromFavourites(any));
  });
}
