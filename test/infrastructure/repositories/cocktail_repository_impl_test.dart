import 'package:cocktail_application/core/errors/exceptions.dart';
import 'package:cocktail_application/core/errors/failures.dart';
import 'package:cocktail_application/core/network/network_info.dart';
import 'package:cocktail_application/domain/entities/cocktail.dart';
import 'package:cocktail_application/domain/entities/cocktail_details.dart';
import 'package:cocktail_application/domain/entities/ingredient.dart';
import 'package:cocktail_application/infrastructure/datasources/cocktail_remote_data_source.dart';
import 'package:cocktail_application/infrastructure/models/cocktail_details_model.dart';
import 'package:cocktail_application/infrastructure/models/cocktail_model.dart';
import 'package:cocktail_application/infrastructure/repositories/cocktail_repository_impl.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockRemoteDataSource extends Mock implements CocktailRemoteDataSource {}

class MockNetworkInfo extends Mock implements NetworkInfo {}

void main() {
  CocktailRepositoryImpl repositoryImpl;
  MockRemoteDataSource mockRemoteDataSource;
  MockNetworkInfo mockNetworkInfo;
  final tCockTailDetailsModel = CocktailDetailsModel(
      idDrink: '1',
      strDrink: 'Test Cocktail',
      strTags: 'Classic,IBA',
      strCategory: 'Ordinary Drink',
      strIBA: 'Unforgettables',
      strAlcoholic: 'Alcholic',
      strGlass: 'Cocktail Glass',
      strInstructions: 'Stir into glass over ice, garnish and serve.',
      strDrinkThumb:
          'https://www.thecocktaildb.com/images/media/drink/qgdu971561574065.jpg',
      strIngredients: [
        'gin',
        'campari',
        'sweet vermouth'
      ],
      strMeasures: [
        '1 oz',
        '1 oz',
        '1 oz',
      ]);

  setUp(() {
    mockRemoteDataSource = MockRemoteDataSource();
    mockNetworkInfo = MockNetworkInfo();
    repositoryImpl = CocktailRepositoryImpl(
      remoteDataSource: mockRemoteDataSource,
      networkInfo: mockNetworkInfo,
    );
  });

  group('getCocktailById', () {
    final tCocktailId = '1';
    final CocktailDetails tCocktailDetails = tCockTailDetailsModel;
    test('should check if the device is online', () async {
      //arrange
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      //act
      repositoryImpl.getCocktailById(tCocktailId);
      //assert
      verify(mockNetworkInfo.isConnected);
    });

    group('device is online', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      });

      test(
          'should return remote data when the call to remote data source is successful.',
          () async {
        //arrange
        when(mockRemoteDataSource.getCocktailById(any))
            .thenAnswer((_) async => tCockTailDetailsModel);
        //act
        final result = await repositoryImpl.getCocktailById(tCocktailId);
        //assert
        verify(mockRemoteDataSource.getCocktailById(tCocktailId));
        expect(result, equals(right(tCocktailDetails)));
      });

      test(
          'should return server failure when the call to remote data source is unsuccessful.',
          () async {
        //arrange
        when(mockRemoteDataSource.getCocktailById(any))
            .thenThrow(ServerException());
        //act
        final result = await repositoryImpl.getCocktailById(tCocktailId);
        //assert
        verify(mockRemoteDataSource.getCocktailById(tCocktailId));
        expect(result, equals(left(ServerFailure())));
      });
    });

    group('device is offline', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);
      });

      test(
          'should return offline failure when the call to remote data source is made when offline.',
          () async {
        //arrange
        when(mockRemoteDataSource.getCocktailById(any))
            .thenThrow(OfflineException());
        //act
        final result = await repositoryImpl.getCocktailById(tCocktailId);
        //assert
        verifyZeroInteractions(mockRemoteDataSource);
        expect(result, equals(left(OfflineFailure())));
      });
    });
  });

  group('getCocktailsByCategory', () {
    final tCocktailCategory = 'test category';
    final tCockTailModelList = [
      CocktailModel(
        idDrink: '1',
        strDrink: 'Test Cocktail',
        strDrinkThumb:
            'https://www.thecocktaildb.com/images/media/drink/qgdu971561574065.jpg',
      )
    ];
    final List<Cocktail> tCocktailList = tCockTailModelList;
    test('should check if the device is online', () async {
      //arrange
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      //act
      repositoryImpl.getCocktailsByCategory(tCocktailCategory);
      //assert
      verify(mockNetworkInfo.isConnected);
    });

    group('device is online', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      });

      test(
          'should return remote data when the call to remote data source is successful.',
          () async {
        //arrange
        when(mockRemoteDataSource.getCocktailsByCategory(any))
            .thenAnswer((_) async => tCockTailModelList);
        //act
        final result =
            await repositoryImpl.getCocktailsByCategory(tCocktailCategory);
        //assert
        verify(mockRemoteDataSource.getCocktailsByCategory(tCocktailCategory));
        expect(result, equals(right(tCocktailList)));
      });

      test(
          'should return server failure when the call to remote data source is unsuccessful.',
          () async {
        //arrange
        when(mockRemoteDataSource.getCocktailsByCategory(any))
            .thenThrow(ServerException());
        //act
        final result =
            await repositoryImpl.getCocktailsByCategory(tCocktailCategory);
        //assert
        verify(mockRemoteDataSource.getCocktailsByCategory(tCocktailCategory));
        expect(result, equals(left(ServerFailure())));
      });
    });

    group('device is offline', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);
      });

      test(
          'should return offline failure when the call to remote data source is made when offline.',
          () async {
        //arrange
        when(mockRemoteDataSource.getCocktailsByCategory(any))
            .thenThrow(OfflineException());
        //act
        final result =
            await repositoryImpl.getCocktailsByCategory(tCocktailCategory);
        //assert
        verifyZeroInteractions(mockRemoteDataSource);
        expect(result, equals(left(OfflineFailure())));
      });
    });
  });

  group('getCocktailsByGlass', () {
    final tCocktailGlass = 'test glass';
    final tCockTailModelList = [
      CocktailModel(
        idDrink: '1',
        strDrink: 'Test Cocktail',
        strDrinkThumb:
            'https://www.thecocktaildb.com/images/media/drink/qgdu971561574065.jpg',
      )
    ];
    final List<Cocktail> tCocktailList = tCockTailModelList;
    test('should check if the device is online', () async {
      //arrange
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      //act
      repositoryImpl.getCocktailsByGlass(tCocktailGlass);
      //assert
      verify(mockNetworkInfo.isConnected);
    });

    group('device is online', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      });

      test(
          'should return remote data when the call to remote data source is successful.',
          () async {
        //arrange
        when(mockRemoteDataSource.getCocktailsByGlass(any))
            .thenAnswer((_) async => tCockTailModelList);
        //act
        final result = await repositoryImpl.getCocktailsByGlass(tCocktailGlass);
        //assert
        verify(mockRemoteDataSource.getCocktailsByGlass(tCocktailGlass));
        expect(result, equals(right(tCocktailList)));
      });

      test(
          'should return server failure when the call to remote data source is unsuccessful.',
          () async {
        //arrange
        when(mockRemoteDataSource.getCocktailsByGlass(any))
            .thenThrow(ServerException());
        //act
        final result = await repositoryImpl.getCocktailsByGlass(tCocktailGlass);
        //assert
        verify(mockRemoteDataSource.getCocktailsByGlass(tCocktailGlass));
        expect(result, equals(left(ServerFailure())));
      });
    });

    group('device is offline', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);
      });

      test(
          'should return offline failure when the call to remote data source is made when offline.',
          () async {
        //arrange
        when(mockRemoteDataSource.getCocktailsByGlass(any))
            .thenThrow(OfflineException());
        //act
        final result = await repositoryImpl.getCocktailsByGlass(tCocktailGlass);
        //assert
        verifyZeroInteractions(mockRemoteDataSource);
        expect(result, equals(left(OfflineFailure())));
      });
    });
  });

  group('getCocktailsByIngredient', () {
    final tCocktailIngreditent = 'test ingredient';
    final tCockTailModelList = [
      CocktailModel(
        idDrink: '1',
        strDrink: 'Test Cocktail',
        strDrinkThumb:
            'https://www.thecocktaildb.com/images/media/drink/qgdu971561574065.jpg',
      )
    ];
    final List<Cocktail> tCocktailList = tCockTailModelList;
    test('should check if the device is online', () async {
      //arrange
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      //act
      repositoryImpl.getCocktailsByIngredient(tCocktailIngreditent);
      //assert
      verify(mockNetworkInfo.isConnected);
    });

    group('device is online', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      });

      test(
          'should return remote data when the call to remote data source is successful.',
          () async {
        //arrange
        when(mockRemoteDataSource.getCocktailsByIngredient(any))
            .thenAnswer((_) async => tCockTailModelList);
        //act
        final result =
            await repositoryImpl.getCocktailsByIngredient(tCocktailIngreditent);
        //assert
        verify(mockRemoteDataSource
            .getCocktailsByIngredient(tCocktailIngreditent));
        expect(result, equals(right(tCocktailList)));
      });

      test(
          'should return server failure when the call to remote data source is unsuccessful.',
          () async {
        //arrange
        when(mockRemoteDataSource.getCocktailsByIngredient(any))
            .thenThrow(ServerException());
        //act
        final result =
            await repositoryImpl.getCocktailsByIngredient(tCocktailIngreditent);
        //assert
        verify(mockRemoteDataSource
            .getCocktailsByIngredient(tCocktailIngreditent));
        expect(result, equals(left(ServerFailure())));
      });
    });

    group('device is offline', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);
      });

      test(
          'should return offline failure when the call to remote data source is made when offline.',
          () async {
        //arrange
        when(mockRemoteDataSource.getCocktailsByIngredient(any))
            .thenThrow(OfflineException());
        //act
        final result =
            await repositoryImpl.getCocktailsByIngredient(tCocktailIngreditent);
        //assert
        verifyZeroInteractions(mockRemoteDataSource);
        expect(result, equals(left(OfflineFailure())));
      });
    });
  });

  group('getCocktailsByName', () {
    final tCocktailName = 'test name';
    final tCockTailDetailsModelList = [tCockTailDetailsModel];
    final List<CocktailDetails> tCocktailDetailsList =
        tCockTailDetailsModelList;
    test('should check if the device is online', () async {
      //arrange
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      //act
      repositoryImpl.getCocktailsByName(tCocktailName);
      //assert
      verify(mockNetworkInfo.isConnected);
    });

    group('device is online', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      });

      test(
          'should return remote data when the call to remote data source is successful.',
          () async {
        //arrange
        when(mockRemoteDataSource.getCocktailsByName(any))
            .thenAnswer((_) async => tCockTailDetailsModelList);
        //act
        final result = await repositoryImpl.getCocktailsByName(tCocktailName);
        //assert
        verify(mockRemoteDataSource.getCocktailsByName(tCocktailName));
        expect(result, equals(right(tCocktailDetailsList)));
      });

      test(
          'should return server failure when the call to remote data source is unsuccessful.',
          () async {
        //arrange
        when(mockRemoteDataSource.getCocktailsByName(any))
            .thenThrow(ServerException());
        //act
        final result = await repositoryImpl.getCocktailsByName(tCocktailName);
        //assert
        verify(mockRemoteDataSource.getCocktailsByName(tCocktailName));
        expect(result, equals(left(ServerFailure())));
      });
    });

    group('device is offline', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);
      });

      test(
          'should return offline failure when the call to remote data source is made when offline.',
          () async {
        //arrange
        when(mockRemoteDataSource.getCocktailsByName(any))
            .thenThrow(OfflineException());
        //act
        final result = await repositoryImpl.getCocktailsByName(tCocktailName);
        //assert
        verifyZeroInteractions(mockRemoteDataSource);
        expect(result, equals(left(OfflineFailure())));
      });
    });
  });

  group('getIngredientsList', () {
    final tIngredientsModelList = [Ingredient(strIngredient1: 'Gin')];
    final List<Ingredient> tIngredientList = tIngredientsModelList;
    test('should check if the device is online', () async {
      //arrange
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      //act
      repositoryImpl.getIngredientsList();
      //assert
      verify(mockNetworkInfo.isConnected);
    });

    group('device is online', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      });

      test(
          'should return remote data when the call to remote data source is successful.',
          () async {
        //arrange
        when(mockRemoteDataSource.getIngredientsList())
            .thenAnswer((_) async => tIngredientsModelList);
        //act
        final result = await repositoryImpl.getIngredientsList();
        //assert
        verify(mockRemoteDataSource.getIngredientsList());
        expect(result, equals(right(tIngredientList)));
      });

      test(
          'should return server failure when the call to remote data source is unsuccessful.',
          () async {
        //arrange
        when(mockRemoteDataSource.getIngredientsList())
            .thenThrow(ServerException());
        //act
        final result = await repositoryImpl.getIngredientsList();
        //assert
        verify(mockRemoteDataSource.getIngredientsList());
        expect(result, equals(left(ServerFailure())));
      });
    });

    group('device is offline', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);
      });

      test(
          'should return offline failure when the call to remote data source is made when offline.',
          () async {
        //arrange
        when(mockRemoteDataSource.getIngredientsList())
            .thenThrow(OfflineException());
        //act
        final result = await repositoryImpl.getIngredientsList();
        //assert
        verifyZeroInteractions(mockRemoteDataSource);
        expect(result, equals(left(OfflineFailure())));
      });
    });
  });

  group('getRandomCocktail', () {
    final CocktailDetails tCocktailDetails = tCockTailDetailsModel;
    test('should check if the device is online', () async {
      //arrange
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      //act
      repositoryImpl.getRandomCocktail();
      //assert
      verify(mockNetworkInfo.isConnected);
    });

    group('device is online', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      });

      test(
          'should return remote data when the call to remote data source is successful.',
          () async {
        //arrange
        when(mockRemoteDataSource.getRandomCocktail())
            .thenAnswer((_) async => tCockTailDetailsModel);
        //act
        final result = await repositoryImpl.getRandomCocktail();
        //assert
        verify(mockRemoteDataSource.getRandomCocktail());
        expect(result, equals(right(tCocktailDetails)));
      });

      test(
          'should return server failure when the call to remote data source is unsuccessful.',
          () async {
        //arrange
        when(mockRemoteDataSource.getRandomCocktail())
            .thenThrow(ServerException());
        //act
        final result = await repositoryImpl.getRandomCocktail();
        //assert
        verify(mockRemoteDataSource.getRandomCocktail());
        expect(result, equals(left(ServerFailure())));
      });
    });

    group('device is offline', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);
      });

      test(
          'should return offline failure when the call to remote data source is made when offline.',
          () async {
        //arrange
        when(mockRemoteDataSource.getRandomCocktail())
            .thenThrow(OfflineException());
        //act
        final result = await repositoryImpl.getRandomCocktail();
        //assert
        verifyZeroInteractions(mockRemoteDataSource);
        expect(result, equals(left(OfflineFailure())));
      });
    });
  });
}
