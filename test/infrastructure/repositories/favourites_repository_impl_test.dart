import 'package:cocktail_application/core/errors/exceptions.dart';
import 'package:cocktail_application/core/errors/failures.dart';
import 'package:cocktail_application/infrastructure/datasources/favourite_cocktails_local_data_source.dart';
import 'package:cocktail_application/infrastructure/models/cocktail_details_model.dart';
import 'package:cocktail_application/infrastructure/repositories/favourites_repository_impl.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockLocalDataSource extends Mock
    implements FavouriteCocktailsLocalDataSource {}

void main() {
  FavouritesRepositoryImpl favouritesRepositoryImpl;
  MockLocalDataSource mockLocalDataSource;
  final tCockTailDetailsModel = CocktailDetailsModel(
      idDrink: '1',
      strDrink: 'Test Cocktail',
      strTags: 'Classic,IBA',
      strCategory: 'Ordinary Drink',
      strIBA: 'Unforgettables',
      strAlcoholic: 'Alcholic',
      strGlass: 'Cocktail Glass',
      strInstructions: 'Stir into glass over ice, garnish and serve.',
      strDrinkThumb:
          'https://www.thecocktaildb.com/images/media/drink/qgdu971561574065.jpg',
      strIngredients: [
        'gin',
        'campari',
        'sweet vermouth'
      ],
      strMeasures: [
        '1 oz',
        '1 oz',
        '1 oz',
      ]);
  final List<CocktailDetailsModel> tCockTailDetailsModelList = [
    tCockTailDetailsModel
  ];

  setUp(() {
    mockLocalDataSource = MockLocalDataSource();
    favouritesRepositoryImpl = FavouritesRepositoryImpl(mockLocalDataSource);
  });

  group('addCocktailToFavourites', () {
    test(
        'should return Cocktail ID when Cocktail is successfully inserted into table',
        () async {
      //arrange
      when(mockLocalDataSource.addCocktailToFavourites(tCockTailDetailsModel))
          .thenAnswer((realInvocation) async => 1);
      //act
      final result = await favouritesRepositoryImpl
          .addCocktailToFavourites(tCockTailDetailsModel);
      //assert
      expect(result, right(1));
      verify(
          mockLocalDataSource.addCocktailToFavourites(tCockTailDetailsModel));
    });

    test(
        'should return {DatabaseFailure} when Cocktail is unsuccessfully inserted into table',
        () async {
      //arrange
      when(mockLocalDataSource.addCocktailToFavourites(tCockTailDetailsModel))
          .thenThrow(EmptyException());
      //act
      final result = await favouritesRepositoryImpl
          .addCocktailToFavourites(tCockTailDetailsModel);
      //assert
      expect(result, equals(left(DatabaseFailure())));
    });
  });

  group('getFavouriteCocktail', () {
    test('should return cocktail when given ID', () async {
      //arrange
      when(mockLocalDataSource.getFavouriteCocktail(any))
          .thenAnswer((realInvocation) async => tCockTailDetailsModel);
      //act
      var result = await favouritesRepositoryImpl.getFavouriteCocktail('id');
      //assert
      expect(result, right(tCockTailDetailsModel));
      verify(mockLocalDataSource.getFavouriteCocktail('id'));
    });

    test(
        'should return {DatabaseFailure} when Cocktail is unsuccessfully retrieved from table',
        () async {
      //arrange
      when(mockLocalDataSource.getFavouriteCocktail(any))
          .thenThrow(EmptyException());
      //act
      final result = await favouritesRepositoryImpl.getFavouriteCocktail('id');
      //assert
      expect(result, equals(left(EmptyFailure())));
    });
  });

  group('getFavouriteCocktails', () {
    test('should return a List of Cocktails', () async {
      //arrange
      when(mockLocalDataSource.getFavouriteCocktails())
          .thenAnswer((realInvocation) async => tCockTailDetailsModelList);
      //act
      var result = await favouritesRepositoryImpl.getFavouriteCocktails();
      //assert
      expect(result, equals(right(tCockTailDetailsModelList)));
      verify(mockLocalDataSource.getFavouriteCocktails());
    });

    test(
        'should return {DatabaseFailure} when Cocktails were unsuccessfully retrieved',
        () async {
      //arrange
      when(mockLocalDataSource.getFavouriteCocktails())
          .thenThrow(EmptyException());
      //act
      final result = await favouritesRepositoryImpl.getFavouriteCocktails();
      //assert
      expect(result, equals(left(EmptyFailure())));
    });
  });

  group('removeCocktailFromFavourites', () {
    test('should return ID when Cocktail is removed', () async {
      //arrange
      when(mockLocalDataSource.removeCocktailFromFavourites(any))
          .thenAnswer((realInvocation) async => 1);
      //act
      var result =
          await favouritesRepositoryImpl.removeCocktailFromFavourites('id');
      //assert
      expect(result, right(1));
      verify(mockLocalDataSource.removeCocktailFromFavourites('id'));
    });

    test(
        'should return {DatabaseFailure} when Cocktail is unsuccessfully deleted from table',
        () async {
      //arrange
      when(mockLocalDataSource.removeCocktailFromFavourites(any))
          .thenThrow(EmptyException());
      //act
      final result =
          await favouritesRepositoryImpl.removeCocktailFromFavourites('id');
      //assert
      expect(result, equals(left(DatabaseFailure())));
    });
  });
}
