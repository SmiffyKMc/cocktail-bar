import 'dart:convert';

import 'package:cocktail_application/domain/entities/cocktail_details_list.dart';
import 'package:cocktail_application/infrastructure/models/cocktail_details_model.dart';
import 'package:cocktail_application/infrastructure/models/cocktail_details_list_model.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../fixtures/fixture_reader.dart';

void main() {
  final tCocktailDetailsListModel =
      CocktailDetailsListModel(cocktailDetailsList: [
    CocktailDetailsModel(
        idDrink: '11003',
        strDrink: 'Negroni',
        strTags: 'IBA,Classic',
        strCategory: 'Ordinary Drink',
        strIBA: 'Unforgettables',
        strAlcoholic: 'Alcoholic',
        strGlass: 'Old-fashioned glass',
        strInstructions: 'Stir into glass over ice, garnish and serve.',
        strDrinkThumb:
            'https://www.thecocktaildb.com/images/media/drink/qgdu971561574065.jpg',
        strIngredients: [
          'Gin',
          'Campari',
          'Sweet Vermouth',
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
        ],
        strMeasures: [
          '1 oz ',
          '1 oz ',
          '1 oz ',
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
        ]),
  ]);

  test('should be a subclass of type CocktailList entity', () async {
    //arrange
    //act
    //assert
    expect(tCocktailDetailsListModel, isA<CocktailDetailsList>());
  });

  group('fromJson', () {
    test('should return a valid model when the JSON ', () async {
      //arrange
      final Map<String, dynamic> jsonMap =
          json.decode(fixture('cocktail_details_list.json'));
      //act
      final result = CocktailDetailsListModel.fromJson(jsonMap);
      //assert
      expect(result, tCocktailDetailsListModel);
    });
  });

  group('toJson', () {
    test('should return a JSON map containing the proper data', () async {
      //arrange
      //act
      final result = tCocktailDetailsListModel.toJson();
      //assert
      final expectedMap = {
        'drinks': [
          {
            "idDrink": '11003',
            "strDrink": 'Negroni',
            "strTags": 'IBA,Classic',
            "strCategory": 'Ordinary Drink',
            "strIBA": 'Unforgettables',
            "strAlcoholic": 'Alcoholic',
            "strGlass": 'Old-fashioned glass',
            "strInstructions": 'Stir into glass over ice, garnish and serve.',
            "strDrinkThumb":
                'https://www.thecocktaildb.com/images/media/drink/qgdu971561574065.jpg',
            "strIngredients": [
              'Gin',
              'Campari',
              'Sweet Vermouth',
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
            ],
            "strMeasures": [
              '1 oz ',
              '1 oz ',
              '1 oz ',
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
            ]
          }
        ]
      };
      expect(result, expectedMap);
    });
  });
}
