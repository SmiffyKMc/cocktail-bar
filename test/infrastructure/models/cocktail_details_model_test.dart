import 'dart:convert';

import 'package:cocktail_application/domain/entities/cocktail_details.dart';
import 'package:cocktail_application/infrastructure/models/cocktail_details_model.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../fixtures/fixture_reader.dart';

void main() {
  final tCocktailDetailsModel = CocktailDetailsModel(
      idDrink: '11003',
      strDrink: 'Negroni',
      strTags: 'IBA,Classic',
      strCategory: 'Ordinary Drink',
      strIBA: 'Unforgettables',
      strAlcoholic: 'Alcoholic',
      strGlass: 'Old-fashioned glass',
      strInstructions: 'Stir into glass over ice, garnish and serve.',
      strDrinkThumb:
          'https://www.thecocktaildb.com/images/media/drink/qgdu971561574065.jpg',
      strIngredients: [
        'Gin',
        'Campari',
        'Sweet Vermouth',
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
      ],
      strMeasures: [
        '1 oz ',
        '1 oz ',
        '1 oz ',
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
      ]);

  test('should be a subclass of Cocktail entity', () async {
    //arrange
    //act
    expect(tCocktailDetailsModel, isA<CocktailDetails>());
    //assert
  });

  group('fromJson', () {
    test('should return a valid model when the JSON ', () async {
      //arrange
      final Map<String, dynamic> jsonMap =
          json.decode(fixture('cocktail_details.json'));
      //act
      final result = CocktailDetailsModel.fromJson(jsonMap);
      //assert
      expect(result, tCocktailDetailsModel);
    });
  });

  group('fromJson from database', () {
    test(
        'should return a valid model when the JSON object is received from the databse',
        () async {
      //arrange
      final Map<String, dynamic> jsonMap =
          json.decode(fixture('cocktail_details_db.json'));
      //act
      final result = CocktailDetailsModel.fromJson(jsonMap);
      //assert
      expect(result.toString(), tCocktailDetailsModel.toString());
    });
  });

  group('toJson', () {
    test('should return a JSON map containing the proper data', () async {
      //arrange
      //act
      final result = tCocktailDetailsModel.toJson();
      //assert
      final expectedMap = {
        "idDrink": '11003',
        "strDrink": 'Negroni',
        "strTags": 'IBA,Classic',
        "strCategory": 'Ordinary Drink',
        "strIBA": 'Unforgettables',
        "strAlcoholic": 'Alcoholic',
        "strGlass": 'Old-fashioned glass',
        "strInstructions": 'Stir into glass over ice, garnish and serve.',
        "strDrinkThumb":
            'https://www.thecocktaildb.com/images/media/drink/qgdu971561574065.jpg',
        "strIngredients":
            '["Gin","Campari","Sweet Vermouth",null,null,null,null,null,null,null,null,null,null,null,null]',
        "strMeasures":
            '["1 oz ","1 oz ","1 oz ",null,null,null,null,null,null,null,null,null,null,null,null]'
      };
      expect(result.toString(), expectedMap.toString());
    });
  });

  group('util functions in model', () {
    test('should build list for ingredients', () async {
      //arrange
      final Map<String, dynamic> jsonMap =
          json.decode(fixture('cocktail_details.json'));
      final key = "strIngredient";
      final tExpectedList = [
        "Gin",
        "Campari",
        "Sweet Vermouth",
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null
      ];
      //act
      List<String> listToReturn = [];
      if (jsonMap.containsKey('${key}1')) {
        for (var i = 1; i < 16; i++) {
          listToReturn.add(jsonMap['$key$i']);
        }
      } else {
        listToReturn = List<String>.from(json.decode(jsonMap['${key}s']));
      }

      //assert
      expect(listToReturn, tExpectedList);
    });
  });

  test('should build list for measures', () async {
    //arrange
    final Map<String, dynamic> jsonMap =
        json.decode(fixture('cocktail_details.json'));
    final key = "strMeasure";
    final tExpectedList = [
      '1 oz ',
      '1 oz ',
      '1 oz ',
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
    ];
    //act
    List<String> listToReturn = [];
    if (jsonMap.containsKey('${key}1')) {
      for (var i = 1; i < 16; i++) {
        listToReturn.add(jsonMap['$key$i']);
      }
    } else {
      listToReturn = List<String>.from(json.decode(jsonMap['${key}s']));
    }

    //assert
    expect(listToReturn, tExpectedList);
  });
}
