import 'dart:convert';

import 'package:cocktail_application/domain/entities/ingredient_list.dart';
import 'package:cocktail_application/infrastructure/models/ingredient_list_model.dart';
import 'package:cocktail_application/infrastructure/models/ingredient_model.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../fixtures/fixture_reader.dart';

void main() {
  final tIngredientListModel = IngredientListModel(ingredientList: [
    IngredientModel(
      strIngredient1: 'Gin',
    ),
  ]);

  test('should be a subclass of type IngredientsList entity', () async {
    //arrange
    //act
    //assert
    expect(tIngredientListModel, isA<IngredientList>());
  });

  group('fromJson', () {
    test('should return a valid model when the JSON ', () async {
      //arrange
      final Map<String, dynamic> jsonMap =
          json.decode(fixture('ingredient_list.json'));
      //act
      final result = IngredientListModel.fromJson(jsonMap);
      //assert
      expect(result, tIngredientListModel);
    });
  });

  group('toJson', () {
    test('should return a JSON map containing the proper data', () async {
      //arrange
      //act
      final result = tIngredientListModel.toJson();
      //assert
      final expectedMap = {
        'drinks': [
          {"strIngredient1": "Gin"}
        ]
      };
      expect(result, expectedMap);
    });
  });
}
