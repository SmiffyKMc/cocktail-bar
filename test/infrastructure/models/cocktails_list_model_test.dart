import 'dart:convert';

import 'package:cocktail_application/domain/entities/cocktails_list.dart';
import 'package:cocktail_application/infrastructure/models/cocktail_model.dart';
import 'package:cocktail_application/infrastructure/models/cocktails_list_model.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../fixtures/fixture_reader.dart';

void main() {
  final tCocktailListModel = CocktailsListModel(cocktailsList: [
    CocktailModel(
      idDrink: '11003',
      strDrink: 'Negroni',
      strDrinkThumb:
          'https://www.thecocktaildb.com/images/media/drink/qgdu971561574065.jpg',
    ),
  ]);

  test('should be a subclass of type CocktailList entity', () async {
    //arrange
    //act
    //assert
    expect(tCocktailListModel, isA<CocktailsList>());
  });

  group('fromJson', () {
    test('should return a valid model when the JSON ', () async {
      //arrange
      final Map<String, dynamic> jsonMap =
          json.decode(fixture('cocktail_list.json'));
      //act
      final result = CocktailsListModel.fromJson(jsonMap);
      //assert
      expect(result, tCocktailListModel);
    });
  });

  group('toJson', () {
    test('should return a JSON map containing the proper data', () async {
      //arrange
      //act
      final result = tCocktailListModel.toJson();
      //assert
      final expectedMap = {
        'drinks': [
          {
            'idDrink': '11003',
            'strDrink': 'Negroni',
            'strDrinkThumb':
                'https://www.thecocktaildb.com/images/media/drink/qgdu971561574065.jpg'
          }
        ]
      };
      expect(result, expectedMap);
    });
  });
}
