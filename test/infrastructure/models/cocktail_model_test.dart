import 'dart:convert';

import 'package:cocktail_application/domain/entities/cocktail.dart';
import 'package:cocktail_application/infrastructure/models/cocktail_model.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../fixtures/fixture_reader.dart';

void main() {
  final tCocktailModel = CocktailModel(
    idDrink: '11003',
    strDrink: 'Negroni',
    strDrinkThumb:
        'https://www.thecocktaildb.com/images/media/drink/qgdu971561574065.jpg',
  );

  test('should be a subclass of Cocktail entity', () async {
    //arrange
    //act
    expect(tCocktailModel, isA<Cocktail>());
    //assert
  });

  group('fromJson', () {
    test('should return a valid model when the JSON ', () async {
      //arrange
      final Map<String, dynamic> jsonMap =
          json.decode(fixture('cocktail.json'));
      //act
      final result = CocktailModel.fromJson(jsonMap);
      //assert
      expect(result, tCocktailModel);
    });
  });

  group('toJson', () {
    test('should return a JSON map containing the proper data', () async {
      //arrange
      //act
      final result = tCocktailModel.toJson();
      //assert
      final expectedMap = {
        "idDrink": '11003',
        "strDrink": 'Negroni',
        "strDrinkThumb":
            'https://www.thecocktaildb.com/images/media/drink/qgdu971561574065.jpg',
      };
      expect(result, expectedMap);
    });
  });
}
