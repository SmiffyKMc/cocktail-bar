import 'dart:convert';

import 'package:cocktail_application/domain/entities/ingredient.dart';
import 'package:cocktail_application/infrastructure/models/ingredient_model.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../fixtures/fixture_reader.dart';

void main() {
  final tIngredientlModel = IngredientModel(strIngredient1: "Gin");

  test('should be a subclass of Cocktail entity', () async {
    //arrange
    //act
    expect(tIngredientlModel, isA<Ingredient>());
    //assert
  });

  group('fromJson', () {
    test('should return a valid model when the JSON ', () async {
      //arrange
      final Map<String, dynamic> jsonMap =
          json.decode(fixture('ingredient.json'));
      //act
      final result = IngredientModel.fromJson(jsonMap);
      //assert
      expect(result, tIngredientlModel);
    });
  });

  group('toJson', () {
    test('should return a JSON map containing the proper data', () async {
      //arrange
      //act
      final result = tIngredientlModel.toJson();
      //assert
      final expectedMap = {"strIngredient1": "Gin"};
      expect(result, expectedMap);
    });
  });
}
