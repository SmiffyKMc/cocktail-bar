import 'package:cocktail_application/infrastructure/models/cocktail_details_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';

Future main() async {
  sqfliteFfiInit();
  DatabaseFactory databaseFactory = databaseFactoryFfi;
  var db = await databaseFactory.openDatabase(inMemoryDatabasePath);
  final tCocktailDetailsModel = CocktailDetailsModel(
      idDrink: '11003',
      strDrink: 'Negroni',
      strTags: 'IBA,Classic',
      strCategory: 'Ordinary Drink',
      strIBA: 'Unforgettables',
      strAlcoholic: 'Alcoholic',
      strGlass: 'Old-fashioned glass',
      strInstructions: 'Stir into glass over ice, garnish and serve.',
      strDrinkThumb:
          'https://www.thecocktaildb.com/images/media/drink/qgdu971561574065.jpg',
      strIngredients: [
        'Gin',
        'Campari',
        'Sweet Vermouth',
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
      ],
      strMeasures: [
        '1 oz ',
        '1 oz ',
        '1 oz ',
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
      ]);
  final String tableFavourites = 'favouritesTable';
  final String columnId = 'idDrink';
  final String columnTitle = 'strDrink';
  final String columnTags = 'strTags';
  final String columnCategory = 'strCategory';
  final String columnIBA = 'strIBA';
  final String columnAlcoholic = 'strAlcoholic';
  final String columnGlass = 'strGlass';
  final String columnInstructions = 'strInstructions';
  final String columnDrinkThumb = 'strDrinkThumb';
  final String columnIngredients = 'strIngredients';
  final String columnMeasures = 'strMeasures';

  await db.execute('''CREATE TABLE $tableFavourites(
          $columnId TEXT PRIMARY KEY, 
          $columnTitle TEXT, 
          $columnTags TEXT, 
          $columnCategory TEXT, 
          $columnIBA TEXT, 
          $columnAlcoholic TEXT, 
          $columnGlass TEXT,
          $columnInstructions TEXT,
          $columnDrinkThumb TEXT,
          $columnIngredients TEXT,
          $columnMeasures TEXT
          )''');

  Future<int> addCocktailDetailToFavourite() async {
    return await db.insert('favouritesTable', tCocktailDetailsModel.toJson(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  test('should insert CocktailDetails into table and print the ID', () async {
    //arrange
    List<Map<String, dynamic>> tExceptedCocktail = [
      {
        'idDrink': '11003',
        'strDrink': 'Negroni',
        'strTags': 'IBA,Classic',
        'strCategory': 'Ordinary Drink',
        'strIBA': 'Unforgettables',
        'strAlcoholic': 'Alcoholic',
        'strGlass': 'Old-fashioned glass',
        'strInstructions': 'Stir into glass over ice, garnish and serve.',
        'strDrinkThumb':
            'https://www.thecocktaildb.com/images/media/drink/qgdu971561574065.jpg',
        'strIngredients':
            '["Gin","Campari","Sweet Vermouth",null,null,null,null,null,null,null,null,null,null,null,null]',
        'strMeasures':
            '["1 oz ","1 oz ","1 oz ",null,null,null,null,null,null,null,null,null,null,null,null]'
      }
    ];
    var id = await addCocktailDetailToFavourite();

    //act
    var result = await db.query('favouritesTable');
    print(result);
    expect(result, tExceptedCocktail);
  });

  test(
      'should retrieve the first CocktailDetails data entry and change CocktailDetails object',
      () async {
    //arrange
    addCocktailDetailToFavourite();
    //act
    var result = await db.query('favouritesTable');
    var resultObj = result.first;
    print(resultObj);
    CocktailDetailsModel cocktailDetailsModel =
        CocktailDetailsModel.fromJson(resultObj);
    //assert
    expect(cocktailDetailsModel, tCocktailDetailsModel);
  });

  test('should remove CocktailDetails data entry from database', () async {
    //arrange
    addCocktailDetailToFavourite();
    //act
    var result = await db.query('favouritesTable');
    //assert
    expect(result.length, 1);

    db.delete('favouritesTable', where: "idDrink = ?", whereArgs: ['11003']);

    var deleteResult = await db.query('favouritesTable');
    //assert
    expect(deleteResult.length, 0);
  });
}
