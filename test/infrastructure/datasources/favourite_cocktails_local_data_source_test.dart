import 'package:cocktail_application/core/errors/exceptions.dart';
import 'package:cocktail_application/infrastructure/datasources/favourite_cocktails_local_data_source_impl.dart';
import 'package:cocktail_application/infrastructure/helpers/database_helper.dart';
import 'package:cocktail_application/infrastructure/models/cocktail_details_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockDatabaseHelper extends Mock implements DatabaseHelper {}

void main() {
  MockDatabaseHelper databaseHelper;
  FavouriteCocktailsLocalDataSourceImpl favouriteCocktailsLocalDataSourceImpl;

  setUp(() {
    databaseHelper = MockDatabaseHelper();
    favouriteCocktailsLocalDataSourceImpl =
        FavouriteCocktailsLocalDataSourceImpl(databaseHelper: databaseHelper);
  });

  final tFavouriteCocktails = [
    CocktailDetailsModel(
        idDrink: '1',
        strDrink: 'Test Cocktail',
        strTags: 'Classic,IBA',
        strCategory: 'Ordinary Drink',
        strIBA: 'Unforgettables',
        strAlcoholic: 'Alcholic',
        strGlass: 'Cocktail Glass',
        strInstructions: 'Stir into glass over ice, garnish and serve.',
        strDrinkThumb:
            'https://www.thecocktaildb.com/images/media/drink/qgdu971561574065.jpg',
        strIngredients: [
          'gin',
          'campari',
          'sweet vermouth'
        ],
        strMeasures: [
          '1 oz',
          '1 oz',
          '1 oz',
        ])
  ];

  group('addCocktailToFavourites', () {
    test('should add a Cocktail to Favourites', () async {
      //arrange
      when(databaseHelper.insertCocktail(any))
          .thenAnswer((realInvocation) async => 1);
      //act
      final result = await favouriteCocktailsLocalDataSourceImpl
          .addCocktailToFavourites(tFavouriteCocktails.first);
      //assert
      verify(databaseHelper.insertCocktail(any));
      expect(result, equals(1));
    });
  });

  group('getFavouriteCocktail', () {
    test('should return a Cocktail from Favourites when given the ID',
        () async {
      //arrange
      when(databaseHelper.getCocktail(any)).thenAnswer(
          (realInvocation) async => [tFavouriteCocktails.first.toJson()]);
      //act
      final result =
          await favouriteCocktailsLocalDataSourceImpl.getFavouriteCocktail('1');
      //assert
      verify(databaseHelper.getCocktail(any));
      expect(result, equals(tFavouriteCocktails.first));
    });

    test('should throw EmptyError if no favourite cocktail exists for given ID',
        () async {
      //arrange
      when(databaseHelper.getCocktail(any)).thenAnswer((_) async => []);
      //act
      final call = favouriteCocktailsLocalDataSourceImpl.getFavouriteCocktail;
      //assert
      expect(() => call('1'), throwsA(isA<EmptyException>()));
    });
  });

  group('getFavouriteCocktails', () {
    test('should return a list of Cocktails from Favourites', () async {
      //arrange
      when(databaseHelper.getCocktails()).thenAnswer(
          (realInvocation) async => [tFavouriteCocktails.first.toJson()]);
      //act
      final result =
          await favouriteCocktailsLocalDataSourceImpl.getFavouriteCocktails();
      //assert
      verify(databaseHelper.getCocktails());
      expect(result, equals(tFavouriteCocktails));
    });

    test('should throw DatabaseError if no favourites exists', () async {
      //arrange
      when(databaseHelper.getCocktails()).thenThrow(DatabaseException());
      //act
      final call = favouriteCocktailsLocalDataSourceImpl.getFavouriteCocktails;
      //assert
      expect(() => call(), throwsA(isA<DatabaseException>()));
      //verifyNoMoreInteractions(databaseHelper);
    });
  });

  group('removeCocktailFromFavourites', () {
    test('should remove a Cocktail from Favourites when given the ID',
        () async {
      //arrange
      when(databaseHelper.removeCocktail(any))
          .thenAnswer((realInvocation) async => 1);
      //act
      final result = await favouriteCocktailsLocalDataSourceImpl
          .removeCocktailFromFavourites('1');
      //assert
      verify(databaseHelper.removeCocktail(any));
      expect(result, equals(1));
    });

    test(
        'should throw DatabaseError if no favourite cocktail exists for given ID',
        () async {
      //arrange
      when(databaseHelper.removeCocktail(any)).thenThrow(DatabaseException());
      //act
      final call =
          favouriteCocktailsLocalDataSourceImpl.removeCocktailFromFavourites;
      //assert
      expect(() => call('1'), throwsA(isA<DatabaseException>()));
    });
  });
}
