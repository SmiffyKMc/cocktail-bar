import 'dart:convert';

import 'package:cocktail_application/core/errors/exceptions.dart';
import 'package:cocktail_application/infrastructure/datasources/cocktail_remote_data_source_impl.dart';
import 'package:cocktail_application/infrastructure/models/cocktail_details_list_model.dart';
import 'package:cocktail_application/infrastructure/models/cocktails_list_model.dart';
import 'package:cocktail_application/infrastructure/models/ingredient_list_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:mockito/mockito.dart';

import '../../fixtures/fixture_reader.dart';

class MockHttpClient extends Mock implements http.Client {}

void main() {
  CocktailRemoteDataSourceImpl dataSourceImpl;
  MockHttpClient mockHttpClient;

  setUp(() {
    mockHttpClient = MockHttpClient();
    dataSourceImpl = CocktailRemoteDataSourceImpl(client: mockHttpClient);
  });

  void _setUpMockHttpClientSuccess200DetailsList() {
    when(mockHttpClient.get(any, headers: anyNamed('headers'))).thenAnswer(
        (_) async => http.Response(fixture('cocktail_details_list.json'), 200));
  }

  void _setUpMockHttpClientEmpty200DetailsList() {
    when(mockHttpClient.get(any, headers: anyNamed('headers'))).thenAnswer(
        (_) async =>
            http.Response(fixture('cocktail_details_list_empty.json'), 200));
  }

  void _setUpMockHttpClientSuccess200List() {
    when(mockHttpClient.get(any, headers: anyNamed('headers'))).thenAnswer(
        (_) async => http.Response(fixture('cocktail_list.json'), 200));
  }

  void _setUpMockHttpClientSuccess200IngredientList() {
    when(mockHttpClient.get(any, headers: anyNamed('headers'))).thenAnswer(
        (_) async => http.Response(fixture('ingredient_list.json'), 200));
  }

  void _setUpMockHttpClientFailed404() {
    when(mockHttpClient.get(any, headers: anyNamed('headers')))
        .thenAnswer((_) async => http.Response('Something went wrong', 404));
  }

  group('getCocktailById', () {
    final tCocktailId = '1';
    final tCocktailDetailsListModel = CocktailDetailsListModel.fromJson(
        json.decode(fixture('cocktail_details_list.json')));
    test(
        'should perform a GET request on a URL with ID being the endpoint and with application/json header',
        () async {
      //arrange
      _setUpMockHttpClientSuccess200DetailsList();
      //act
      dataSourceImpl.getCocktailById(tCocktailId);
      //assert
      verify(mockHttpClient.get(
        'https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=$tCocktailId',
        headers: {'Content-Type': 'application/json'},
      ));
    });

    test(
        'should return CocktailDetails when the response code is 200 (success)',
        () async {
      //arrange
      _setUpMockHttpClientSuccess200DetailsList();
      //act
      final result = await dataSourceImpl.getCocktailById(tCocktailId);
      //assert
      expect(
          result, equals(tCocktailDetailsListModel.cocktailDetailsList.first));
    });

    test(
        'should throw a ServerException when the response code is 404 or other.',
        () async {
      //arrange
      _setUpMockHttpClientFailed404();
      //act
      final call = dataSourceImpl.getCocktailById;
      //assert
      expect(() => call(tCocktailId), throwsA(isA<ServerException>()));
    });

    test(
        'should throw a ServerException when the response code is 200 but empty with a StateException.',
        () async {
      //arrange
      _setUpMockHttpClientEmpty200DetailsList();
      //act
      final call = dataSourceImpl.getCocktailById;
      //assert
      expect(() => call(tCocktailId), throwsA(isA<ServerException>()));
    });
  });

  group('getCocktailsByCategory', () {
    final tCocktailCategory = 'test category';
    final tCocktailModelList =
        CocktailsListModel.fromJson(json.decode(fixture('cocktail_list.json')));

    test(
        'should perform a GET request on a URL with Category being the endpoint and with application/json header',
        () async {
      //arrange
      _setUpMockHttpClientSuccess200List();
      //act
      dataSourceImpl.getCocktailsByCategory(tCocktailCategory);
      //assert
      verify(mockHttpClient.get(
        'https://www.thecocktaildb.com/api/json/v1/1/filter.php?c=$tCocktailCategory',
        headers: {'Content-Type': 'application/json'},
      ));
    });

    test(
        'should return a List of Cocktails when the response code is 200 (success)',
        () async {
      //arrange
      _setUpMockHttpClientSuccess200List();
      //act
      final result =
          await dataSourceImpl.getCocktailsByCategory(tCocktailCategory);
      //assert
      expect(result, equals(tCocktailModelList.cocktailsList));
    });

    test(
        'should throw a ServerException when the response code is 404 or other.',
        () async {
      //arrange
      _setUpMockHttpClientFailed404();
      //act
      final call = dataSourceImpl.getCocktailsByCategory;
      //assert
      expect(() => call(tCocktailCategory), throwsA(isA<ServerException>()));
    });
  });

  group('getCocktailsByGlass', () {
    final tCocktailGlass = 'test glass';
    final tCocktailModelList =
        CocktailsListModel.fromJson(json.decode(fixture('cocktail_list.json')));

    test(
        'should perform a GET method on a URL with Glass being the endpoint and with application/json',
        () async {
      //arrange
      _setUpMockHttpClientSuccess200List();
      //act
      dataSourceImpl.getCocktailsByGlass(tCocktailGlass);
      //assert
      verify(
        mockHttpClient.get(
            'https://www.thecocktaildb.com/api/json/v1/1/filter.php?g=$tCocktailGlass',
            headers: {'Content-Type': 'application/json'}),
      );
    });

    test(
        'should return a List of Cocktails when the response code is 200 (success)',
        () async {
      //arrange
      _setUpMockHttpClientSuccess200List();
      //act
      final result = await dataSourceImpl.getCocktailsByGlass(tCocktailGlass);
      //assert
      expect(result, equals(tCocktailModelList.cocktailsList));
    });

    test(
        'should throw a ServerException when the response code is 404 or other.',
        () async {
      //arrange
      _setUpMockHttpClientFailed404();
      //act
      final call = dataSourceImpl.getCocktailsByGlass;
      //assert
      expect(() => call(tCocktailGlass), throwsA(isA<ServerException>()));
    });
  });

  group('getCocktailsByIngredient', () {
    final tCocktailIngredient = 'test ingredient';
    final tCocktailModelList =
        CocktailsListModel.fromJson(json.decode(fixture('cocktail_list.json')));

    test(
        'should perform a GET method on a URL with Glass being the endpoint and with application/json',
        () async {
      //arrange
      _setUpMockHttpClientSuccess200List();
      //act
      dataSourceImpl.getCocktailsByIngredient(tCocktailIngredient);
      //assert
      verify(
        mockHttpClient.get(
            'https://www.thecocktaildb.com/api/json/v1/1/filter.php?i=$tCocktailIngredient',
            headers: {'Content-Type': 'application/json'}),
      );
    });

    test(
        'should return a List of Cocktails when the response code is 200 (success)',
        () async {
      //arrange
      _setUpMockHttpClientSuccess200List();
      //act
      final result =
          await dataSourceImpl.getCocktailsByIngredient(tCocktailIngredient);
      //assert
      expect(result, equals(tCocktailModelList.cocktailsList));
    });

    test(
        'should throw a ServerException when the response code is 404 or other.',
        () async {
      //arrange
      _setUpMockHttpClientFailed404();
      //act
      final call = dataSourceImpl.getCocktailsByIngredient;
      //assert
      expect(() => call(tCocktailIngredient), throwsA(isA<ServerException>()));
    });
  });

  group('getCocktailsByName', () {
    final tCocktailName = 'test name';
    final tCocktailDetailsModelList = CocktailDetailsListModel.fromJson(
        json.decode(fixture('cocktail_details_list.json')));

    test(
        'should perform a GET method on a URL with Glass being the endpoint and with application/json',
        () async {
      //arrange
      _setUpMockHttpClientSuccess200DetailsList();
      //act
      dataSourceImpl.getCocktailsByName(tCocktailName);
      //assert
      verify(
        mockHttpClient.get(
            'https://www.thecocktaildb.com/api/json/v1/1/search.php?s=$tCocktailName',
            headers: {'Content-Type': 'application/json'}),
      );
    });

    test(
        'should return a List of Cocktails when the response code is 200 (success)',
        () async {
      //arrange
      _setUpMockHttpClientSuccess200DetailsList();
      //act
      final result = await dataSourceImpl.getCocktailsByName(tCocktailName);
      //assert
      expect(result, equals(tCocktailDetailsModelList.cocktailDetailsList));
    });

    test(
        'should throw a ServerException when the response code is 404 or other.',
        () async {
      //arrange
      _setUpMockHttpClientFailed404();
      //act
      final call = dataSourceImpl.getCocktailsByName;
      //assert
      expect(() => call(tCocktailName), throwsA(isA<ServerException>()));
    });
  });

  group('getIngredientsList', () {
    final tIngredientModelList = IngredientListModel.fromJson(
        json.decode(fixture('ingredient_list.json')));

    test(
        'should perform a GET method on a URL with Glass being the endpoint and with application/json',
        () async {
      //arrange
      _setUpMockHttpClientSuccess200IngredientList();
      //act
      dataSourceImpl.getIngredientsList();
      //assert
      verify(
        mockHttpClient.get(
            'https://www.thecocktaildb.com/api/json/v1/1/list.php?i=list',
            headers: {'Content-Type': 'application/json'}),
      );
    });

    test(
        'should return a List of Ingredients when the response code is 200 (success)',
        () async {
      //arrange
      _setUpMockHttpClientSuccess200IngredientList();
      //act
      final result = await dataSourceImpl.getIngredientsList();
      //assert
      expect(result, equals(tIngredientModelList.ingredientList));
    });

    test(
        'should throw a ServerException when the response code is 404 or other.',
        () async {
      //arrange
      _setUpMockHttpClientFailed404();
      //act
      final call = dataSourceImpl.getIngredientsList;
      //assert
      expect(() => call(), throwsA(isA<ServerException>()));
    });
  });

  group('getRandomCocktail', () {
    final tCocktailDetailsModel = CocktailDetailsListModel.fromJson(
        json.decode(fixture('cocktail_details_list.json')));
    test(
        'should perform a GET request on a URL with ID being the endpoint and with application/json header',
        () async {
      //arrange
      _setUpMockHttpClientSuccess200DetailsList();
      //act
      dataSourceImpl.getRandomCocktail();
      //assert
      verify(mockHttpClient.get(
        'https://www.thecocktaildb.com/api/json/v1/1/random.php',
        headers: {'Content-Type': 'application/json'},
      ));
    });

    test(
        'should return CocktailDetails when the response code is 200 (success)',
        () async {
      //arrange
      _setUpMockHttpClientSuccess200DetailsList();
      //act
      final result = await dataSourceImpl.getRandomCocktail();
      print(tCocktailDetailsModel.toString());
      //assert
      expect(result, equals(tCocktailDetailsModel.cocktailDetailsList.first));
    });

    test(
        'should throw a ServerException when the response code is 404 or other.',
        () async {
      //arrange
      _setUpMockHttpClientFailed404();
      //act
      final call = dataSourceImpl.getRandomCocktail;
      //assert
      expect(() => call(), throwsA(isA<ServerException>()));
    });

    test(
        'should throw a ServerException when the response code is 200 but empty with a StateException.',
        () async {
      //arrange
      _setUpMockHttpClientEmpty200DetailsList();
      //act
      final call = dataSourceImpl.getRandomCocktail;
      //assert
      expect(() => call(), throwsA(isA<ServerException>()));
    });
  });
}
