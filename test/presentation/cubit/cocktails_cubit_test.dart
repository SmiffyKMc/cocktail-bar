import 'package:bloc_test/bloc_test.dart';
import 'package:cocktail_application/core/errors/failures.dart';
import 'package:cocktail_application/domain/entities/cocktail.dart';
import 'package:cocktail_application/domain/usecases/get_cocktails_from_api/get_cocktails_by_category.dart'
    as cat;
import 'package:cocktail_application/domain/usecases/get_cocktails_from_api/get_cocktails_by_glass.dart'
    as gla;
import 'package:cocktail_application/domain/usecases/get_cocktails_from_api/get_cocktails_by_ingredient.dart'
    as ing;
import 'package:cocktail_application/presentation/cubit/cocktails_cubit.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockGetCocktailsByCategory extends Mock
    implements cat.GetCocktailsByCategory {}

class MockGetCocktailsByGlass extends Mock implements gla.GetCocktailsByGlass {}

class MockGetCocktailsByIngredient extends Mock
    implements ing.GetCocktailsByIngredient {}

void main() {
  MockGetCocktailsByCategory mockGetCocktailsByCategory;
  MockGetCocktailsByGlass mockGetCocktailsByGlass;
  MockGetCocktailsByIngredient mockGetCocktailsByIngredient;

  setUp(() {
    mockGetCocktailsByIngredient = MockGetCocktailsByIngredient();
    mockGetCocktailsByGlass = MockGetCocktailsByGlass();
    mockGetCocktailsByCategory = MockGetCocktailsByCategory();
  });

  CocktailsCubit buildCocktailCubit() {
    return CocktailsCubit(
      getCocktailsByCategoryRep: mockGetCocktailsByCategory,
      getCocktailsByGlassRep: mockGetCocktailsByGlass,
      getCocktailsByIngredientRep: mockGetCocktailsByIngredient,
    );
  }

  final tCocktail = [
    Cocktail(
      idDrink: '1',
      strDrink: 'Test Cocktail',
      strDrinkThumb:
          'https://www.thecocktaildb.com/images/media/drink/qgdu971561574065.jpg',
    )
  ];

  group('getCocktaisByCategory Cubit call', () {
    blocTest(
      'should emit [CocktailsLoading, CocktailsLoaded] when a successful call is made.',
      build: () => buildCocktailCubit(),
      act: (CocktailsCubit cubit) {
        when(mockGetCocktailsByCategory.call(cat.Params(category: 'category')))
            .thenAnswer((_) async => right(tCocktail));
        cubit.getCocktailsByCategory('category');
      },
      expect: () => [
        CocktailsLoading(),
        CocktailsLoaded(
          tCocktail,
        )
      ],
    );

    blocTest(
      'should emit [CocktailsLoading, CocktailsError] when a unsuccessful call is made.',
      build: () => buildCocktailCubit(),
      act: (CocktailsCubit cubit) {
        when(mockGetCocktailsByCategory.call(cat.Params(category: 'category')))
            .thenAnswer((_) async => left(ServerFailure()));
        cubit.getCocktailsByCategory('category');
      },
      expect: () =>
          [CocktailsLoading(), CocktailsError(CocktailsCubit.ERROR_MESSAGE)],
    );
  });

  group('getCocktaisByGlass Cubit call', () {
    blocTest(
      'should emit [CocktailsLoading, CocktailsLoaded] when a successful call is made.',
      build: () => buildCocktailCubit(),
      act: (CocktailsCubit cubit) {
        when(mockGetCocktailsByGlass.call(gla.Params(glass: 'glass')))
            .thenAnswer((_) async => right(tCocktail));
        cubit.getCocktailsByGlass('glass');
      },
      expect: () => [CocktailsLoading(), CocktailsLoaded(tCocktail)],
    );

    blocTest(
      'should emit [CocktailsLoading, CocktailsError] when a unsuccessful call is made.',
      build: () => buildCocktailCubit(),
      act: (CocktailsCubit cubit) {
        when(mockGetCocktailsByGlass.call(gla.Params(glass: 'glass')))
            .thenAnswer((_) async => left(ServerFailure()));
        cubit.getCocktailsByGlass('glass');
      },
      expect: () =>
          [CocktailsLoading(), CocktailsError(CocktailsCubit.ERROR_MESSAGE)],
    );
  });

  group('getCocktaisByIngredient Cubit call', () {
    blocTest(
      'should emit [CocktailsLoading, CocktailsLoaded] when a successful call is made.',
      build: () => buildCocktailCubit(),
      act: (CocktailsCubit cubit) {
        when(mockGetCocktailsByIngredient
                .call(ing.Params(ingredient: 'ingredient')))
            .thenAnswer((_) async => right(tCocktail));
        cubit.getCocktailsByIngredient('ingredient');
      },
      expect: () => [CocktailsLoading(), CocktailsLoaded(tCocktail)],
    );

    blocTest(
      'should emit [CocktailsLoading, CocktailsError] when a unsuccessful call is made.',
      build: () => buildCocktailCubit(),
      act: (CocktailsCubit cubit) {
        when(mockGetCocktailsByIngredient
                .call(ing.Params(ingredient: 'ingredient')))
            .thenAnswer((_) async => left(ServerFailure()));
        cubit.getCocktailsByIngredient('ingredient');
      },
      expect: () =>
          [CocktailsLoading(), CocktailsError(CocktailsCubit.ERROR_MESSAGE)],
    );
  });
}
