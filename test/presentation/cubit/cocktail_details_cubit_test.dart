import 'package:bloc_test/bloc_test.dart';
import 'package:cocktail_application/core/errors/failures.dart';
import 'package:cocktail_application/core/usecases/usecase.dart';
import 'package:cocktail_application/domain/entities/cocktail_details.dart';
import 'package:cocktail_application/domain/usecases/get_cocktails_from_api/get_cocktail_by_id.dart'
    as id;
import 'package:cocktail_application/domain/usecases/get_cocktails_from_api/get_cocktails_by_name.dart'
    as nam;
import 'package:cocktail_application/domain/usecases/get_cocktails_from_api/get_random_cocktail.dart'
    as ran;
import 'package:cocktail_application/presentation/cubit/cocktail_details_cubit.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockGetCocktailsById extends Mock implements id.GetCocktailById {}

class MockGetCocktailsByName extends Mock implements nam.GetCocktailsByName {}

class MockGetCocktailsByRandom extends Mock implements ran.GetRandomCocktail {}

void main() {
  MockGetCocktailsById mockGetCocktailsById;
  MockGetCocktailsByName mockGetCocktailsByName;
  MockGetCocktailsByRandom mockGetCocktailsByRandom;

  setUp(() {
    mockGetCocktailsById = MockGetCocktailsById();
    mockGetCocktailsByName = MockGetCocktailsByName();
    mockGetCocktailsByRandom = MockGetCocktailsByRandom();
  });

  CocktailDetailsCubit buildCocktailDetailsCubit() {
    return CocktailDetailsCubit(
      getCocktailByNameRep: mockGetCocktailsByName,
      getRandomCocktailRep: mockGetCocktailsByRandom,
      getCocktailByIdRep: mockGetCocktailsById,
    );
  }

  final tId = '1';
  final tName = 'gin';
  final tCocktail = [
    CocktailDetails(
        idDrink: '1',
        strDrink: 'Test Cocktail',
        strTags: 'Classic,IBA',
        strCategory: 'Ordinary Drink',
        strIBA: 'Unforgettables',
        strAlcoholic: 'Alcholic',
        strGlass: 'Cocktail Glass',
        strInstructions: 'Stir into glass over ice, garnish and serve.',
        strDrinkThumb:
            'https://www.thecocktaildb.com/images/media/drink/qgdu971561574065.jpg',
        strIngredients: [
          'gin',
          'campari',
          'sweet vermouth'
        ],
        strMeasures: [
          '1 oz',
          '1 oz',
          '1 oz',
        ])
  ];

  group('getCocktaisById Cubit call', () {
    blocTest(
      'should emit [CocktailDetailsLoading, CocktailDetailsLoaded] when a successful call is made.',
      build: () => buildCocktailDetailsCubit(),
      act: (CocktailDetailsCubit cubit) {
        when(mockGetCocktailsById.call(id.Params(id: tId)))
            .thenAnswer((_) async => right(tCocktail.first));
        cubit.getCocktailsById(tId);
      },
      expect: () => [
        CocktailDetailsLoading(),
        CocktailDetailsLoaded(
          tCocktail,
        )
      ],
    );

    blocTest(
      'should emit [CocktailDetailsLoading, CocktailDetailsError] when a unsuccessful call is made.',
      build: () => buildCocktailDetailsCubit(),
      act: (CocktailDetailsCubit cubit) {
        when(mockGetCocktailsById.call(id.Params(id: tId)))
            .thenAnswer((_) async => left(ServerFailure()));
        cubit.getCocktailsById(tId);
      },
      expect: () => [
        CocktailDetailsLoading(),
        CocktailDetailsError(CocktailDetailsCubit.ERROR_MESSAGE)
      ],
    );
  });

  group('getCocktaisByName Cubit call', () {
    blocTest(
      'should emit [CocktailDetailsLoading, CocktailDetailsLoaded] when a successful call is made.',
      build: () => buildCocktailDetailsCubit(),
      act: (CocktailDetailsCubit cubit) {
        when(mockGetCocktailsByName.call(nam.Params(name: tName)))
            .thenAnswer((_) async => right(tCocktail));
        cubit.getCocktailsByName(tName);
      },
      expect: () => [
        CocktailDetailsLoading(),
        CocktailDetailsLoaded(
          tCocktail,
        )
      ],
    );

    blocTest(
      'should emit [CocktailDetailsLoading, CocktailDetailsError] when a unsuccessful call is made.',
      build: () => buildCocktailDetailsCubit(),
      act: (CocktailDetailsCubit cubit) {
        when(mockGetCocktailsByName.call(nam.Params(name: tName)))
            .thenAnswer((_) async => left(ServerFailure()));
        cubit.getCocktailsByName(tName);
      },
      expect: () => [
        CocktailDetailsLoading(),
        CocktailDetailsError(CocktailDetailsCubit.ERROR_MESSAGE)
      ],
    );
  });

  group('getRandomCocktail Cubit call', () {
    blocTest(
      'should emit [CocktailDetailsLoading, CocktailDetailsLoaded] when a successful call is made.',
      build: () => buildCocktailDetailsCubit(),
      act: (CocktailDetailsCubit cubit) {
        when(mockGetCocktailsByRandom.call(NoParams()))
            .thenAnswer((_) async => right(tCocktail.first));
        cubit.getRandomCocktail();
      },
      expect: () => [
        CocktailDetailsLoading(),
        CocktailDetailsLoaded(
          tCocktail,
        )
      ],
    );

    blocTest(
      'should emit [CocktailDetailsLoading, CocktailDetailsError] when a unsuccessful call is made.',
      build: () => buildCocktailDetailsCubit(),
      act: (CocktailDetailsCubit cubit) {
        when(mockGetCocktailsByRandom.call(NoParams()))
            .thenAnswer((_) async => left(ServerFailure()));
        cubit.getRandomCocktail();
      },
      expect: () => [
        CocktailDetailsLoading(),
        CocktailDetailsError(CocktailDetailsCubit.ERROR_MESSAGE)
      ],
    );
  });
}
