import 'package:bloc_test/bloc_test.dart';
import 'package:cocktail_application/core/errors/failures.dart';
import 'package:cocktail_application/core/usecases/usecase.dart';
import 'package:cocktail_application/domain/usecases/get_favourite_cocktails/add_cocktail_to_favourites.dart'
    as add;
import 'package:cocktail_application/domain/usecases/get_favourite_cocktails/get_favourite_cocktails.dart'
    as all;
import 'package:cocktail_application/domain/usecases/get_favourite_cocktails/get_favourtie_cocktail.dart'
    as one;
import 'package:cocktail_application/domain/usecases/get_favourite_cocktails/remove_cocktail_from_favourites.dart'
    as rem;
import 'package:cocktail_application/infrastructure/models/cocktail_details_model.dart';
import 'package:cocktail_application/presentation/cubit/favourites_cubit.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockGetFavouriteCocktails extends Mock
    implements all.GetFavouriteCocktails {}

class MockGetFavouriteCocktail extends Mock
    implements one.GetFavouriteCocktail {}

class MockAddCocktailToFavourites extends Mock
    implements add.AddCocktailToFavourites {}

class MockRemoveCocktailFromFavourites extends Mock
    implements rem.RemoveCocktailFromFavourites {}

void main() {
  MockGetFavouriteCocktails mockGetFavouriteCocktails;
  MockGetFavouriteCocktail mockGetFavouriteCocktail;
  MockAddCocktailToFavourites mockAddCocktailToFavourites;
  MockRemoveCocktailFromFavourites mockRemoveCocktailFromFavourites;

  final tId = '1';
  final tCocktailDetails = CocktailDetailsModel(
      idDrink: '1',
      strDrink: 'Test Cocktail',
      strTags: 'Classic,IBA',
      strCategory: 'Ordinary Drink',
      strIBA: 'Unforgettables',
      strAlcoholic: 'Alcholic',
      strGlass: 'Cocktail Glass',
      strInstructions: 'Stir into glass over ice, garnish and serve.',
      strDrinkThumb:
          'https://www.thecocktaildb.com/images/media/drink/qgdu971561574065.jpg',
      strIngredients: [
        'gin',
        'campari',
        'sweet vermouth'
      ],
      strMeasures: [
        '1 oz',
        '1 oz',
        '1 oz',
      ]);
  final tFavouriteCocktails = [
    CocktailDetailsModel(
        idDrink: '1',
        strDrink: 'Test Cocktail',
        strTags: 'Classic,IBA',
        strCategory: 'Ordinary Drink',
        strIBA: 'Unforgettables',
        strAlcoholic: 'Alcholic',
        strGlass: 'Cocktail Glass',
        strInstructions: 'Stir into glass over ice, garnish and serve.',
        strDrinkThumb:
            'https://www.thecocktaildb.com/images/media/drink/qgdu971561574065.jpg',
        strIngredients: [
          'gin',
          'campari',
          'sweet vermouth'
        ],
        strMeasures: [
          '1 oz',
          '1 oz',
          '1 oz',
        ])
  ];

  setUp(() {
    mockGetFavouriteCocktails = MockGetFavouriteCocktails();
    mockGetFavouriteCocktail = MockGetFavouriteCocktail();
    mockAddCocktailToFavourites = MockAddCocktailToFavourites();
    mockRemoveCocktailFromFavourites = MockRemoveCocktailFromFavourites();
  });

  FavouritesCubit buildFavourtiesCubit() {
    return FavouritesCubit(
      getFavouriteCocktailsRep: mockGetFavouriteCocktails,
      getFavouriteCocktailRep: mockGetFavouriteCocktail,
      addCocktailToFavouritesRep: mockAddCocktailToFavourites,
      removeCocktailFromFavouritesRep: mockRemoveCocktailFromFavourites,
    );
  }

  group('getFavouriteCocktails Cubit call', () {
    blocTest(
      'should emit [FavouritesLoading, FavouritesLoaded] when a successful call is made.',
      build: () => buildFavourtiesCubit(),
      act: (FavouritesCubit cubit) {
        when(mockGetFavouriteCocktails.call(NoParams()))
            .thenAnswer((_) async => right(tFavouriteCocktails));
        cubit.getFavouriteCocktails();
      },
      expect: () => [
        FavouritesLoading(),
        FavouritesLoaded(
          tFavouriteCocktails,
        )
      ],
    );

    blocTest(
      'should emit [FavouritesLoading, FavouritesError] when a unsuccessful call is made.',
      build: () => buildFavourtiesCubit(),
      act: (FavouritesCubit cubit) {
        when(mockGetFavouriteCocktails.call(NoParams()))
            .thenAnswer((_) async => left(DatabaseFailure()));
        cubit.getFavouriteCocktails();
      },
      expect: () =>
          [FavouritesLoading(), FavouritesError(FavouritesCubit.ERROR_MESSAGE)],
    );
  });

  group('getFavouriteCocktail Cubit call', () {
    blocTest(
      'should emit [FavouritesLoading, FavouritesLoaded] when a successful call is made.',
      build: () => buildFavourtiesCubit(),
      act: (FavouritesCubit cubit) {
        when(mockGetFavouriteCocktail.call(one.Params(cocktailId: tId)))
            .thenAnswer((_) async => right(tFavouriteCocktails.first));
        cubit.getFavouriteCocktail(tId);
      },
      expect: () => [
        FavouritesLoading(),
        FavouritesLoaded(
          tFavouriteCocktails,
        )
      ],
    );

    blocTest(
      'should emit [FavouritesLoading, FavouritesError] when a unsuccessful call is made.',
      build: () => buildFavourtiesCubit(),
      act: (FavouritesCubit cubit) {
        when(mockGetFavouriteCocktail.call(one.Params(cocktailId: tId)))
            .thenAnswer((_) async => left(DatabaseFailure()));
        cubit.getFavouriteCocktail(tId);
      },
      expect: () =>
          [FavouritesLoading(), FavouritesError(FavouritesCubit.ERROR_MESSAGE)],
    );
  });

  group('addCocktailToFavourites Cubit call', () {
    blocTest(
      'should emit [FavouritesLoading, FavouriteAdded] when a successful call is made.',
      build: () => buildFavourtiesCubit(),
      act: (FavouritesCubit cubit) {
        when(mockAddCocktailToFavourites
                .call(add.Params(cocktailDetails: tCocktailDetails)))
            .thenAnswer((_) async => right(1));
        cubit.addCocktailToFavourites(tCocktailDetails);
      },
      expect: () => [FavouritesLoading(), FavouriteAdded(1)],
    );

    blocTest(
      'should emit [FavouritesLoading, FavouritesError] when a unsuccessful call is made.',
      build: () => buildFavourtiesCubit(),
      act: (FavouritesCubit cubit) {
        when(mockAddCocktailToFavourites
                .call(add.Params(cocktailDetails: tCocktailDetails)))
            .thenAnswer((_) async => left(DatabaseFailure()));
        cubit.addCocktailToFavourites(tCocktailDetails);
      },
      expect: () =>
          [FavouritesLoading(), FavouritesError(FavouritesCubit.ERROR_MESSAGE)],
    );
  });

  group('removeCocktailFromFavourites Cubit call', () {
    blocTest(
      'should emit [FavouritesLoading, FavouriteRemoved] when a successful call is made.',
      build: () => buildFavourtiesCubit(),
      act: (FavouritesCubit cubit) {
        when(mockRemoveCocktailFromFavourites.call(rem.Params(cocktailId: tId)))
            .thenAnswer((_) async => right(1));
        cubit.removeCocktailFromFavourites(tId);
      },
      expect: () => [FavouritesLoading(), FavouriteRemoved(1)],
    );

    blocTest(
      'should emit [FavouritesLoading, FavouritesError] when a unsuccessful call is made.',
      build: () => buildFavourtiesCubit(),
      act: (FavouritesCubit cubit) {
        when(mockRemoveCocktailFromFavourites.call(rem.Params(cocktailId: tId)))
            .thenAnswer((_) async => left(DatabaseFailure()));
        cubit.removeCocktailFromFavourites(tId);
      },
      expect: () =>
          [FavouritesLoading(), FavouritesError(FavouritesCubit.ERROR_MESSAGE)],
    );
  });
}
