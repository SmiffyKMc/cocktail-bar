class StringManipulator {
  StringManipulator();
  String _removeNumbersFromString(String stringToClean) {
    final stringToBeReturned = stringToClean.replaceAll(RegExp(r"[0-9]"), '');
    return stringToBeReturned;
  }

  List<String> _splitStringByDot(String stringToSplit) {
    final listOfSplittedStrings =
        stringToSplit.split('.').map((sentence) => sentence.trim()).toList();
    final listWithClearedWhitespace = <String>[];
    listOfSplittedStrings.forEach((sentence) {
      if (sentence.length > 0) listWithClearedWhitespace.add('$sentence.');
    });
    return listWithClearedWhitespace;
  }

  List<String> _capitalizeFirstWord(List<String> stringToSplit) {
    return stringToSplit
        .map((sentence) => sentence[0].toUpperCase() + sentence.substring(1))
        .toList();
  }

  List<String> formatInstructions(String stringToFormat) {
    final formatedString = _removeNumbersFromString(stringToFormat);
    final stringSplitToList = _splitStringByDot(formatedString);
    return _capitalizeFirstWord(stringSplitToList);
  }
}
