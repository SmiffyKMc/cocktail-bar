import 'package:cocktail_application/presentation/pages/cocktails_home/cocktails_home_page.dart';
import 'package:flutter/material.dart';

import 'injection.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  configureDependencies();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        buttonTheme: ButtonThemeData(
          buttonColor: Color(0xfff5a21c),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18.0),
          ),
        ),
        textTheme: TextTheme(
          bodyText1: TextStyle(
            color: Colors.white,
            fontSize: 12,
          ),
          bodyText2: TextStyle(
            color: Colors.white,
            fontSize: 16,
          ),
          button: TextStyle(
            color: Colors.white,
            fontSize: 14,
          ),
          headline1: TextStyle(
            color: Colors.white,
            fontSize: 20,
          ),
          headline2: TextStyle(
            fontSize: 20,
          ),
        ),
      ),
      home: CocktailsHomePage(),
    );
  }
}
