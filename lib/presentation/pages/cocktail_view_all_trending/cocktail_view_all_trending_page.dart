import 'package:cocktail_application/domain/usecases/get_cocktails_from_api/get_cocktails_by_category.dart';
import 'package:cocktail_application/domain/usecases/get_cocktails_from_api/get_cocktails_by_glass.dart';
import 'package:cocktail_application/domain/usecases/get_cocktails_from_api/get_cocktails_by_ingredient.dart';
import 'package:cocktail_application/presentation/cubit/cocktails_cubit.dart';
import 'package:cocktail_application/presentation/widgets/staggered_cocktail_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../injection.dart';

class CocktailViewAllTrendingPage extends StatelessWidget {
  const CocktailViewAllTrendingPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: BlocProvider(
        create: (context) => CocktailsCubit(
          getCocktailsByCategoryRep: getIt<GetCocktailsByCategory>(),
          getCocktailsByGlassRep: getIt<GetCocktailsByGlass>(),
          getCocktailsByIngredientRep: getIt<GetCocktailsByIngredient>(),
        ),
        child: Scaffold(
          backgroundColor: Color(0xff040404),
          body: Container(
            child: BlocProvider<CocktailsCubit>(
              create: (context) =>
                  getIt<CocktailsCubit>()..getCocktailsByCategory('Cocktail'),
              child: BlocBuilder<CocktailsCubit, CocktailsState>(
                builder: (context, state) {
                  if (state is CocktailsLoading) {
                    return Center(child: CircularProgressIndicator());
                  } else if (state is CocktailsLoaded) {
                    final cocktailList = state.cocktailList;
                    return StaggeredCocktailList(
                      cocktailList: cocktailList,
                      shuffle: true,
                    );
                  } else {
                    return Text('Error');
                  }
                },
              ),
            ),
          ),
        ),
      ),
    );
  }
}
