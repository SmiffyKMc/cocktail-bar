import 'package:cocktail_application/presentation/pages/cocktail_receipes/cocktail_receipes_page.dart';
import 'package:cocktail_application/presentation/pages/favourite_cocktails/favourite_cocktails_page.dart';
import 'package:cocktail_application/presentation/pages/shake_for_cocktail/shake_for_cocktail.dart';
import 'package:flutter/material.dart';

class CocktailsHomePage extends StatefulWidget {
  const CocktailsHomePage({Key key}) : super(key: key);

  @override
  _CocktailsHomePageState createState() => _CocktailsHomePageState();
}

class _CocktailsHomePageState extends State<CocktailsHomePage> {
  int _currentIndex = 1;
  bool isShakeForCocktailVisible = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        index: _currentIndex,
        children: [
          ShakeForCocktail(
            isVisible: isShakeForCocktailVisible,
          ),
          const CocktailsReceipesPage(),
          FavouriteCocktailsPage(),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex:
            _currentIndex, // this will be set when a new tab is tapped
        backgroundColor: Color(0xff131415),
        selectedItemColor: Color(0xfff5a41d),
        unselectedItemColor: Color(0xff717171),
        onTap: (int index) {
          setState(() {
            isShakeForCocktailVisible = index == 0 ? true : false;
            _currentIndex = index;
          });
        },
        showUnselectedLabels: false,
        items: [
          BottomNavigationBarItem(
              icon: const Icon(Icons.no_drinks), label: 'SHAKE'),
          BottomNavigationBarItem(
              icon: const Icon(Icons.list_alt_rounded), label: 'RECEIPES'),
          BottomNavigationBarItem(
              icon: const Icon(Icons.favorite), label: 'FAVOURITES')
        ],
      ),
    );
  }
}
