import 'dart:async';

import 'package:animated_widgets/animated_widgets.dart';
import 'package:animated_widgets/widgets/shake_animated_widget.dart';
import 'package:cocktail_application/presentation/pages/cocktail_details/cocktail_details_page.dart';
import 'package:flutter/material.dart';
import 'package:just_audio/just_audio.dart';
import 'package:lottie/lottie.dart';
import 'package:sensors/sensors.dart';

class ShakeForCocktail extends StatefulWidget {
  ShakeForCocktail({Key key, this.isVisible}) : super(key: key);
  final bool isVisible;

  @override
  _ShakeForCocktailState createState() => _ShakeForCocktailState();
}

class _ShakeForCocktailState extends State<ShakeForCocktail> {
  AudioPlayer player;
  bool isShaking = false;
  List<StreamSubscription<dynamic>> _streamSubscriptions =
      <StreamSubscription<dynamic>>[];

  @override
  Widget build(BuildContext context) {
    if (_streamSubscriptions.isEmpty && widget.isVisible) subscribeStreams();

    return Container(
      child: buildShakerInfo(),
    );
  }

  Widget buildShakerInfo() {
    return Container(
      color: Color(0xff131415),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ShakeAnimatedWidget(
            enabled: true,
            duration: Duration(milliseconds: 1850),
            shakeAngle: Rotation.deg(z: 30),
            curve: Curves.linear,
            child: Lottie.asset(
              'assets/shaking.json',
              fit: BoxFit.fitHeight,
            ),
          ),
          Text('Shake your phone for a cocktail!'),
        ],
      ),
    );
  }

  void buildRandomCocktail() {
    stopStreams();
    if (mounted) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => CocktailDetailsPage(),
            settings: RouteSettings(
              arguments: '-1',
            ),
          ),
        ).then((value) {
          isShaking = false;
          subscribeStreams();
        });
      });
    }
  }

  @override
  void dispose() {
    player.dispose();
    stopStreams();
    super.dispose();
  }

  /// Stops all streams and clears list of stream subs.
  void stopStreams() {
    for (StreamSubscription<dynamic> subscription in _streamSubscriptions) {
      print(subscription.toString());
      subscription.cancel();
    }
    _streamSubscriptions.clear();
  }

  void subscribeStreams() {
    // Don't want to add multiple subscriptions to the streams, so triple checking
    if (_streamSubscriptions.isEmpty && widget.isVisible) {
      _streamSubscriptions.add(
          userAccelerometerEvents.listen((UserAccelerometerEvent event) async {
        if (mounted && !isShaking) {
          if (event.x > 30 || event.y > 30) {
            await player.setAsset('assets/audio/cocktailshaker.m4a');
            player.play();
            setState(() {
              isShaking = true;
              buildRandomCocktail();
            });
          }
        }
      }));
    }
  }

  @override
  void initState() {
    player = AudioPlayer();
    subscribeStreams();
    super.initState();
  }
}
