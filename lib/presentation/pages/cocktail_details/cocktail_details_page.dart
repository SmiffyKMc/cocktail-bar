import 'package:cocktail_application/domain/entities/cocktail_details.dart';
import 'package:cocktail_application/domain/usecases/get_cocktails_from_api/get_cocktail_by_id.dart';
import 'package:cocktail_application/domain/usecases/get_cocktails_from_api/get_random_cocktail.dart';
import 'package:cocktail_application/presentation/cubit/cocktail_details_cubit.dart';
import 'package:cocktail_application/presentation/cubit/favourites_cubit.dart';
import 'package:cocktail_application/presentation/widgets/detail_steps.dart';
import 'package:cocktail_application/presentation/widgets/details_ingredients.dart';
import 'package:cocktail_application/presentation/widgets/loading_cocktail.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:transparent_image/transparent_image.dart';

import '../../../injection.dart';

class CocktailDetailsPage extends StatefulWidget {
  const CocktailDetailsPage({Key key}) : super(key: key);

  @override
  _CocktailDetailsPageState createState() => _CocktailDetailsPageState();
}

class _CocktailDetailsPageState extends State<CocktailDetailsPage> {
  bool isFavourite;
  @override
  Widget build(BuildContext context) {
    final String cocktailId = ModalRoute.of(context).settings.arguments;
    return Container(
      child: BlocProvider(
        create: (context) => CocktailDetailsCubit(
          getCocktailByIdRep: getIt<GetCocktailById>(),
          getCocktailByNameRep: null,
          getRandomCocktailRep: getIt<GetRandomCocktail>(),
        ),
        child: Scaffold(
          backgroundColor: Color(0xff040404),
          body: Container(
            child: BlocProvider<CocktailDetailsCubit>(
              create: (context) {
                if (cocktailId != '-1') {
                  return getIt<CocktailDetailsCubit>()
                    ..getCocktailsById(cocktailId);
                } else {
                  return getIt<CocktailDetailsCubit>()..getRandomCocktail();
                }
              },
              child: BlocBuilder<CocktailDetailsCubit, CocktailDetailsState>(
                builder: (context, state) {
                  if (state is CocktailDetailsLoading) {
                    return Center(child: CircularProgressIndicator());
                  } else if (state is CocktailDetailsLoaded) {
                    final cocktailDetails = state.cocktailDetailsList.first;
                    return Container(
                      child: ListView(
                        children: [
                          FeaturedCocktail(
                            cocktailDetails: cocktailDetails,
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 50, bottom: 2),
                            height: 50,
                            child: Text(
                              'Ingredients',
                              style: Theme.of(context).textTheme.headline1,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 50),
                            child: DetailsIngredients(
                              cocktailDetails: cocktailDetails,
                            ),
                          ),
                          Container(
                            margin:
                                EdgeInsets.only(left: 50, top: 10, bottom: 2),
                            height: 50,
                            child: Text(
                              'Steps',
                              style: Theme.of(context).textTheme.headline1,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 50, right: 10),
                            child: DetailSteps(
                              cocktailDetails: cocktailDetails,
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 100),
                            child:
                                BlocConsumer<FavouritesCubit, FavouritesState>(
                              bloc: getIt<FavouritesCubit>()
                                ..getFavouriteCocktail(cocktailDetails.idDrink),
                              listener: (context, state) {
                                print(state);
                              },
                              builder: (context, state) {
                                if (state is FavouritesLoading) {
                                  return LoadingCocktail();
                                } else if (state is FavouritesLoaded) {
                                  final cocktailId = cocktailDetails.idDrink;
                                  return ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      primary: Color(0xfff5a41d),
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(32.0),
                                      ),
                                    ),
                                    onPressed: () {
                                      getIt<FavouritesCubit>()
                                        ..removeCocktailFromFavourites(
                                            cocktailId);
                                      setState(() {});
                                    },
                                    child: Text(
                                      'Remove from My Drinks',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  );
                                } else if (state is FavouritesEmpty) {
                                  return OutlinedButton(
                                    onPressed: () {
                                      getIt<FavouritesCubit>()
                                        ..addCocktailToFavourites(
                                            cocktailDetails);
                                      setState(() {});
                                    },
                                    child: Text(
                                      '+ Add to My Drinks',
                                      style:
                                          TextStyle(color: Color(0xfff5a41d)),
                                    ),
                                    style: ElevatedButton.styleFrom(
                                      side: BorderSide(
                                          width: 1.0, color: Color(0xfff5a41d)),
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(32.0),
                                      ),
                                    ),
                                  );
                                } else {
                                  // (state is CocktailsError)
                                  return Center(
                                      child: Text(
                                    'No internet connection Error',
                                    style: TextStyle(color: Colors.white),
                                  ));
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                    );
                    //! This does not work like the Bloc, we need to cacth the error on
                    //! the Cubit and also add a listener.
                  } else if (state is CocktailDetailsError) {
                    return Center(
                        child: Text(
                      'No internet connection Error',
                      style: TextStyle(color: Colors.white),
                    ));
                  } else {
                    return Text('Error');
                  }
                },
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class FeaturedCocktail extends StatelessWidget {
  final CocktailDetails cocktailDetails;
  const FeaturedCocktail({
    Key key,
    this.cocktailDetails,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        FadeInImage.memoryNetwork(
          placeholder: kTransparentImage,
          image: cocktailDetails.strDrinkThumb,
        ),
        Positioned(
          bottom: 0,
          child: Opacity(
              opacity: 0.8,
              child: Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.bottomCenter,
                      end: Alignment.topCenter,
                      colors: [Colors.black, Colors.black12],
                      stops: [0.19, 1]),
                ),
                margin: EdgeInsets.only(top: 60),
                padding: EdgeInsets.only(left: 20, top: 10, bottom: 10),
                child: Column(
                  children: [
                    Container(
                      alignment: Alignment.center,
                      width: MediaQuery.of(context).size.width,
                      child: Text(
                        cocktailDetails.strDrink.toUpperCase(),
                        maxLines: 3,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 24,
                        ),
                      ),
                    )
                  ],
                ),
              )),
        ),
      ],
    );
  }
}
