import 'dart:async';

import 'package:cocktail_application/injection.dart';
import 'package:cocktail_application/presentation/cubit/favourites_cubit.dart';
import 'package:cocktail_application/presentation/widgets/loading_cocktail.dart';
import 'package:cocktail_application/presentation/widgets/staggered_cocktail_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';

class FavouriteCocktailsPage extends StatefulWidget {
  FavouriteCocktailsPage({Key key}) : super(key: key);

  @override
  _FavouriteCocktailsPageState createState() => _FavouriteCocktailsPageState();
}

class _FavouriteCocktailsPageState extends State<FavouriteCocktailsPage> {
  FutureOr refresh(dynamic value) {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff040404),
      body: Center(
        child: Container(
          child: BlocConsumer<FavouritesCubit, FavouritesState>(
            bloc: getIt<FavouritesCubit>()..getFavouriteCocktails(),
            listener: (context, state) {
              print(state);
            },
            builder: (context, state) {
              if (state is FavouritesLoading) {
                return LoadingCocktail();
              } else if (state is FavouritesLoaded) {
                final favouriteCocktails = state.cocktailList;
                return StaggeredCocktailList(
                  cocktailList: favouriteCocktails,
                  shuffle: false,
                  refresh: () => refresh,
                );
              } else if (state is FavouritesEmpty) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Lottie.asset(
                      'assets/empty-fav.json',
                      fit: BoxFit.fitHeight,
                    ),
                    Text('No Favourites Available')
                  ],
                );
              } else {
                // (state is CocktailsError)
                return Text('Error');
              }
            },
          ),
        ),
      ),
    );
  }
}
