import 'dart:math';

import 'package:cocktail_application/domain/entities/cocktail.dart';
import 'package:cocktail_application/domain/usecases/get_cocktails_from_api/get_cocktails_by_category.dart';
import 'package:cocktail_application/domain/usecases/get_cocktails_from_api/get_cocktails_by_glass.dart';
import 'package:cocktail_application/domain/usecases/get_cocktails_from_api/get_cocktails_by_ingredient.dart';
import 'package:cocktail_application/presentation/cubit/cocktails_cubit.dart';
import 'package:cocktail_application/presentation/pages/cocktail_details/cocktail_details_page.dart';
import 'package:cocktail_application/presentation/pages/cocktail_view_all_trending/cocktail_view_all_trending_page.dart';
import 'package:cocktail_application/presentation/widgets/cocktails_horizontal_row.dart';
import 'package:cocktail_application/presentation/widgets/loading_cocktail.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:transparent_image/transparent_image.dart';

import '../../../injection.dart';

class CocktailsReceipesPage extends StatelessWidget {
  const CocktailsReceipesPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: BlocProvider(
        create: (context) => CocktailsCubit(
          getCocktailsByCategoryRep: getIt<GetCocktailsByCategory>(),
          getCocktailsByGlassRep: getIt<GetCocktailsByGlass>(),
          getCocktailsByIngredientRep: getIt<GetCocktailsByIngredient>(),
        ),
        child: Scaffold(
          backgroundColor: Color(0xff040404),
          body: Container(
            child: BlocProvider<CocktailsCubit>(
              create: (context) =>
                  getIt<CocktailsCubit>()..getCocktailsByCategory('Cocktail'),
              child: BlocBuilder<CocktailsCubit, CocktailsState>(
                builder: (context, state) {
                  if (state is CocktailsLoading) {
                    //return Center(child: CircularProgressIndicator());
                    return LoadingCocktail();
                  } else if (state is CocktailsLoaded) {
                    final featuredCocktail = state.cocktailList
                        .getRange(0, state.cocktailList.length)
                        .elementAt(Random().nextInt(state.cocktailList.length));
                    return Container(
                      child: ListView(
                        children: [
                          GestureDetector(
                            onTap: () => goToCocktailDetails(
                                context, featuredCocktail.idDrink),
                            child: Container(
                              child: FeaturedCocktail(
                                cocktail: featuredCocktail,
                              ),
                            ),
                          ),
                          buildTrendingCocktails(context, state.cocktailList),
                          buildExploreCocktails(context, state.cocktailList),
                          buildFavouritesCocktails(context, state.cocktailList),
                        ],
                      ),
                    );
                  } else {
                    // (state is CocktailsError)
                    return Text('Error');
                  }
                },
              ),
            ),
          ),
        ),
      ),
    );
  }

  void goToCocktailDetails(BuildContext context, String cocktailId) {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => CocktailDetailsPage(),
          settings: RouteSettings(arguments: cocktailId)),
    );
  }

  Widget buildTrendingCocktails(
      BuildContext context, List<Cocktail> cocktailList) {
    return Expanded(
      child: GestureDetector(
        child: Container(
          height: MediaQuery.of(context).size.height / 3.1,
          child: Column(
            children: [
              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Trending Cocktails',
                    style: Theme.of(context).textTheme.headline1,
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => CocktailViewAllTrendingPage(),
                        ),
                      );
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color: Color(0xfff5a41d),
                          borderRadius: BorderRadius.all(Radius.circular(15))),
                      margin: EdgeInsets.only(right: 8.0),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20.0, vertical: 2.0),
                        child: Text(
                          'View All',
                        ),
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 14,
              ),
              CocktailsHorizontalRow(
                cocktailsList: cocktailList.getRange(6, 11).toList(),
                goToCocktailDetails: goToCocktailDetails,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildExploreCocktails(
      BuildContext context, List<Cocktail> cocktailList) {
    return Expanded(
      child: Container(
        height: MediaQuery.of(context).size.height / 3.1,
        child: Column(
          children: [
            SizedBox(
              height: 4,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Explore',
                  style: Theme.of(context).textTheme.headline1,
                ),
              ],
            ),
            SizedBox(
              height: 14,
            ),
            CocktailsHorizontalRow(
              cocktailsList: cocktailList.getRange(0, 5).toList(),
              goToCocktailDetails: goToCocktailDetails,
            ),
          ],
        ),
      ),
    );
  }

  Widget buildFavouritesCocktails(
      BuildContext context, List<Cocktail> cocktailList) {
    return Expanded(
      child: Container(
        height: MediaQuery.of(context).size.height / 3.1,
        child: Column(
          children: [
            SizedBox(
              height: 4,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Favourites',
                  style: Theme.of(context).textTheme.headline1,
                ),
              ],
            ),
            SizedBox(
              height: 18,
            ),
            CocktailsHorizontalRow(
              cocktailsList: cocktailList.getRange(12, 14).toList(),
              goToCocktailDetails: goToCocktailDetails,
            ),
          ],
        ),
      ),
    );
  }
}

class FeaturedCocktail extends StatelessWidget {
  final Cocktail cocktail;
  const FeaturedCocktail({
    Key key,
    this.cocktail,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        FadeInImage.memoryNetwork(
          placeholder: kTransparentImage,
          image: cocktail.strDrinkThumb,
        ),
        Positioned(
          child: Container(
            decoration: BoxDecoration(
              color: Colors.black12,
            ),
            margin: EdgeInsets.only(top: 60, bottom: 20),
            padding: EdgeInsets.only(left: 20, top: 10, bottom: 10),
            child: Column(
              children: [
                Container(
                    width: MediaQuery.of(context).size.width / 2,
                    child: Text(
                      'Featured Receipe',
                      style: Theme.of(context).textTheme.headline1,
                    )),
                Container(
                    width: MediaQuery.of(context).size.width / 2,
                    child: Text(
                      cocktail.strDrink,
                      maxLines: 3,
                      style: TextStyle(color: Colors.white, fontSize: 30),
                    ))
              ],
            ),
          ),
        ),
        Positioned(
          child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                  color: Colors.red),
              padding: EdgeInsets.all(4),
              child: Icon(Icons.local_fire_department,
                  size: 30, color: Colors.white)),
          right: 15,
          top: 15,
        )
      ],
    );
  }
}
