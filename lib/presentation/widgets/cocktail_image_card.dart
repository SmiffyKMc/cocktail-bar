import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';

class CocktailImageCard extends StatelessWidget {
  final String networkImage;
  final double cardWidth;
  final double cardHeight;
  const CocktailImageCard(
      {Key key, this.networkImage, this.cardWidth, this.cardHeight})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(12),
          topRight: Radius.circular(12),
          bottomRight: Radius.circular(12)),
      child: Container(
        width: cardWidth,
        height: cardHeight,
        child: Stack(children: [
          Center(child: CircularProgressIndicator()),
          buildFadeInImage()
        ]),
      ),
    );
  }

  Widget buildFadeInImage() {
    return FadeInImage.memoryNetwork(
      placeholder: kTransparentImage,
      image: networkImage,
    );
  }
}
