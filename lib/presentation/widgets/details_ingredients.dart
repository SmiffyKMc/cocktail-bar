import 'package:cocktail_application/domain/entities/cocktail_details.dart';
import 'package:flutter/material.dart';

class DetailsIngredients extends StatelessWidget {
  final CocktailDetails cocktailDetails;
  const DetailsIngredients({Key key, this.cocktailDetails}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<TableRow> tableRows = [];

    const rowSpacer = TableRow(children: [
      SizedBox(
        height: 8,
      ),
      SizedBox(
        height: 8,
      )
    ]);
    for (var i = 0; i < cocktailDetails.strIngredients.length; i++) {
      if (cocktailDetails.strIngredients[i] != null) {
        final tableRow = TableRow(children: [
          TableCell(
            child: Text(
              '${cocktailDetails.strMeasures[i] ?? ""}',
              style: Theme.of(context).textTheme.bodyText2,
            ),
          ),
          TableCell(
            child: Text(
              ' ${cocktailDetails.strIngredients[i] ?? ""}',
              style: Theme.of(context).textTheme.bodyText2,
            ),
          ),
        ]);
        tableRows.add(tableRow);
        tableRows.add(rowSpacer);
      }
    }
    return Table(
      columnWidths: {
        0: FlexColumnWidth(4),
        1: FlexColumnWidth(10),
      },
      children: tableRows,
    );
  }
}
