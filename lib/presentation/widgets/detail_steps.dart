import 'package:cocktail_application/core/util/string_manipulator.dart';
import 'package:cocktail_application/domain/entities/cocktail_details.dart';
import 'package:flutter/material.dart';

class DetailSteps extends StatelessWidget {
  final CocktailDetails cocktailDetails;
  const DetailSteps({Key key, this.cocktailDetails}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<TableRow> tableRows = [];
    List<String> instructions = new StringManipulator()
        .formatInstructions(cocktailDetails.strInstructions);
    const rowSpacer = TableRow(children: [
      SizedBox(
        height: 8,
      ),
      SizedBox(
        height: 8,
      )
    ]);
    for (var i = 0; i < instructions.length; i++) {
      final instruction = instructions[i];
      if (instruction != null) {
        final tableRow = TableRow(children: [
          TableCell(
            child: Text(
              '${i + 1}',
              style: TextStyle(color: Color(0xfff5a21c), fontSize: 17),
            ),
          ),
          TableCell(
            child: Text(instruction),
          ),
        ]);
        tableRows.add(tableRow);
        tableRows.add(rowSpacer);
      }
    }
    return Table(
      columnWidths: {
        0: FlexColumnWidth(1),
        1: FlexColumnWidth(8),
      },
      children: tableRows,
    );
  }
}
