import 'package:cocktail_application/presentation/pages/cocktail_details/cocktail_details_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:transparent_image/transparent_image.dart';

class StaggeredCocktailList extends StatefulWidget {
  final List<dynamic> cocktailList;
  final bool shuffle;
  final Function refresh;
  const StaggeredCocktailList({
    Key key,
    @required this.cocktailList,
    this.shuffle,
    this.refresh,
  }) : super(key: key);

  @override
  _StaggeredCocktailListState createState() => _StaggeredCocktailListState();
}

class _StaggeredCocktailListState extends State<StaggeredCocktailList> {
  @override
  Widget build(BuildContext context) {
    if (widget.shuffle) widget.cocktailList.shuffle();
    return Container(
      child: StaggeredGridView.countBuilder(
          crossAxisCount: 2,
          crossAxisSpacing: 10,
          mainAxisSpacing: 12,
          itemCount: widget.cocktailList.length,
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => CocktailDetailsPage(),
                    settings: RouteSettings(
                      arguments: widget.cocktailList[index].idDrink,
                    ),
                  ),
                ).then(widget.refresh());
              },
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.transparent,
                    borderRadius: BorderRadius.all(Radius.circular(15))),
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                  child: FadeInImage.memoryNetwork(
                    placeholder: kTransparentImage,
                    image: widget.cocktailList[index].strDrinkThumb,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            );
          },
          staggeredTileBuilder: (index) {
            return StaggeredTile.count(1, index.isEven ? 1.2 : 1.8);
          }),
    );
  }
}
