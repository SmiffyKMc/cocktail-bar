import 'package:cocktail_application/domain/entities/cocktail.dart';
import 'package:cocktail_application/presentation/widgets/cocktail_image_card.dart';
import 'package:flutter/material.dart';

class CocktailsHorizontalRow extends StatefulWidget {
  final List<Cocktail> cocktailsList;
  final Function goToCocktailDetails;
  const CocktailsHorizontalRow(
      {Key key, this.cocktailsList, this.goToCocktailDetails})
      : super(key: key);

  @override
  _CocktailsHorizontalRowState createState() => _CocktailsHorizontalRowState();
}

class _CocktailsHorizontalRowState extends State<CocktailsHorizontalRow>
    with TickerProviderStateMixin {
  AnimationController _controller;
  bool isFavourite = false;
  List<String> favs = ['A1'];

  @override
  void initState() {
    super.initState();

    _controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 800));
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: widget.cocktailsList
              .map(
                (cocktail) => buildCocktailDisplayCard(context, cocktail),
              )
              .toList(),
        ),
      ),
    );
  }

  Widget buildCocktailDisplayCard(BuildContext context, Cocktail cocktail) {
    final cardWidth = MediaQuery.of(context).size.width / 2.3;
    final cardHeight = MediaQuery.of(context).size.height / 5;
    return GestureDetector(
      onTap: () => widget.goToCocktailDetails(context, cocktail.idDrink),
      child: Container(
        margin: EdgeInsets.only(left: 4, right: 8),
        width: cardWidth,
        height: cardHeight,
        child: Column(
          children: [
            Stack(children: [
              CocktailImageCard(
                cardWidth: cardWidth,
                networkImage: cocktail.strDrinkThumb,
              ),
            ]),
            Expanded(
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 4),
                width: cardWidth,
                child: Text(
                  cocktail.strDrink,
                  textAlign: TextAlign.left,
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void setAsFavourite() {
    isFavourite = true;
    _controller.forward(from: 0);
  }

  void unSetAsFavourite() {
    isFavourite = false;
    _controller..reverse(from: 75);
  }
}
