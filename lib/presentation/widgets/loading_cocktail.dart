import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class LoadingCocktail extends StatelessWidget {
  const LoadingCocktail({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Container(
          child: Lottie.asset('assets/loading.json', fit: BoxFit.fitHeight
              //height: 70,
              ),
        ),
      ),
    );
  }
}
