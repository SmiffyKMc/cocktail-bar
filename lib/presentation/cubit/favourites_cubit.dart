import 'package:bloc/bloc.dart';
import 'package:cocktail_application/core/errors/failures.dart';
import 'package:cocktail_application/core/usecases/usecase.dart';
import 'package:cocktail_application/domain/entities/cocktail_details.dart';
import '../../domain/usecases/get_favourite_cocktails/add_cocktail_to_favourites.dart'
    as add;
import '../../domain/usecases/get_favourite_cocktails/get_favourite_cocktails.dart'
    as getAll;
import '../../domain/usecases/get_favourite_cocktails/get_favourtie_cocktail.dart'
    as getOne;
import '../../domain/usecases/get_favourite_cocktails/remove_cocktail_from_favourites.dart'
    as rem;
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';

part 'favourites_state.dart';

@injectable
class FavouritesCubit extends Cubit<FavouritesState> {
  //!TODO: Change error
  static const String ERROR_MESSAGE = 'Something wrong has happened, try again';
  final getAll.GetFavouriteCocktails getFavouriteCocktailsRep;
  final getOne.GetFavouriteCocktail getFavouriteCocktailRep;
  final add.AddCocktailToFavourites addCocktailToFavouritesRep;
  final rem.RemoveCocktailFromFavourites removeCocktailFromFavouritesRep;

  FavouritesCubit({
    @required this.getFavouriteCocktailsRep,
    @required this.getFavouriteCocktailRep,
    @required this.addCocktailToFavouritesRep,
    @required this.removeCocktailFromFavouritesRep,
  }) : super(FavouritesEmpty());

  Future<void> getFavouriteCocktails() async {
    emit(FavouritesLoading());
    final favouriteCocktails = await getFavouriteCocktailsRep(NoParams());
    await Future<void>.delayed(const Duration(milliseconds: 50));
    favouriteCocktails.fold(
      (left) => left is EmptyFailure
          ? emit(FavouritesEmpty())
          : emit(FavouritesError(ERROR_MESSAGE)),
      (right) => emit(FavouritesLoaded(right)),
    );
  }

  Future<void> getFavouriteCocktail(String id) async {
    emit(FavouritesLoading());
    final favouriteCocktail =
        await getFavouriteCocktailRep(getOne.Params(cocktailId: id));
    favouriteCocktail.fold(
      (left) => left is EmptyFailure
          ? emit(FavouritesEmpty())
          : emit(FavouritesError(ERROR_MESSAGE)),
      (right) => emit(FavouritesLoaded([right])),
    );
  }

  Future<void> addCocktailToFavourites(CocktailDetails cocktailDetails) async {
    emit(FavouritesLoading());
    final cocktailId = await addCocktailToFavouritesRep(
        add.Params(cocktailDetails: cocktailDetails));
    cocktailId.fold(
      (left) => emit(FavouritesError(ERROR_MESSAGE)),
      (right) => emit(FavouriteAdded(right)),
    );
  }

  Future<void> removeCocktailFromFavourites(String id) async {
    emit(FavouritesLoading());
    final cocktailId =
        await removeCocktailFromFavouritesRep(rem.Params(cocktailId: id));
    cocktailId.fold(
      (left) => emit(FavouritesError(ERROR_MESSAGE)),
      (right) => emit(FavouriteRemoved(right)),
    );
  }
}
