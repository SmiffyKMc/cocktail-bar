part of 'cocktails_cubit.dart';

abstract class CocktailsState extends Equatable {
  const CocktailsState();

  @override
  List<Object> get props => [];
}

class CocktailsEmpty extends CocktailsState {}

class CocktailsLoading extends CocktailsState {
  const CocktailsLoading();
}

class CocktailsError extends CocktailsState {
  final String message;
  const CocktailsError(
    this.message,
  );

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is CocktailsError && o.message == message;
  }

  @override
  int get hashCode => message.hashCode;
}

class CocktailsLoaded extends CocktailsState {
  final List<Cocktail> cocktailList;
  const CocktailsLoaded(
    this.cocktailList,
  );

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is CocktailsLoaded && listEquals(o.cocktailList, cocktailList);
  }

  @override
  int get hashCode => cocktailList.hashCode;
}
