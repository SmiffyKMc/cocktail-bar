import 'package:bloc/bloc.dart';
import 'package:injectable/injectable.dart';
import '../../domain/entities/cocktail.dart';
import 'package:cocktail_application/domain/usecases/get_cocktails_from_api/get_cocktails_by_category.dart'
    as categoryRepo;
import 'package:cocktail_application/domain/usecases/get_cocktails_from_api/get_cocktails_by_glass.dart'
    as glassRepo;
import 'package:cocktail_application/domain/usecases/get_cocktails_from_api/get_cocktails_by_ingredient.dart'
    as ingredientRepo;
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

part 'cocktails_state.dart';

@injectable
class CocktailsCubit extends Cubit<CocktailsState> {
  static const String ERROR_MESSAGE = 'Something wrong has happened, try again';
  final categoryRepo.GetCocktailsByCategory getCocktailsByCategoryRep;
  final glassRepo.GetCocktailsByGlass getCocktailsByGlassRep;
  final ingredientRepo.GetCocktailsByIngredient getCocktailsByIngredientRep;

  CocktailsCubit(
      {@required this.getCocktailsByCategoryRep,
      @required this.getCocktailsByGlassRep,
      @required this.getCocktailsByIngredientRep})
      : super(CocktailsEmpty());

  Future<void> getCocktailsByCategory(String category) async {
    emit(CocktailsLoading());
    final cocktails = await getCocktailsByCategoryRep(
        categoryRepo.Params(category: category));
    cocktails.fold(
      (left) => emit(CocktailsError(ERROR_MESSAGE)),
      (right) => emit(CocktailsLoaded(right)),
    );
  }

  Future<void> getCocktailsByGlass(String glass) async {
    emit(CocktailsLoading());
    final cocktails =
        await getCocktailsByGlassRep(glassRepo.Params(glass: glass));
    cocktails.fold(
      (left) => emit(CocktailsError(ERROR_MESSAGE)),
      (right) => emit(CocktailsLoaded(right)),
    );
  }

  Future<void> getCocktailsByIngredient(String ingredient) async {
    emit(CocktailsLoading());
    final cocktails = await getCocktailsByIngredientRep(
        ingredientRepo.Params(ingredient: ingredient));
    cocktails.fold(
      (left) => emit(CocktailsError(ERROR_MESSAGE)),
      (right) => emit(CocktailsLoaded(right)),
    );
  }
}
