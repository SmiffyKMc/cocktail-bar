import 'package:bloc/bloc.dart';
import 'package:cocktail_application/domain/entities/cocktail_details.dart';
import 'package:cocktail_application/domain/usecases/get_cocktails_from_api/get_cocktail_by_id.dart'
    as idRepo;
import 'package:cocktail_application/domain/usecases/get_cocktails_from_api/get_cocktails_by_name.dart'
    as nam;
import 'package:cocktail_application/domain/usecases/get_cocktails_from_api/get_random_cocktail.dart'
    as ran;
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';

import '../../core/usecases/usecase.dart';

part 'cocktail_details_state.dart';

@injectable
class CocktailDetailsCubit extends Cubit<CocktailDetailsState> {
  static const String ERROR_MESSAGE = 'Something wrong has happened, try again';

  final idRepo.GetCocktailById getCocktailByIdRep;
  final nam.GetCocktailsByName getCocktailByNameRep;
  final ran.GetRandomCocktail getRandomCocktailRep;

  CocktailDetailsCubit({
    @required this.getCocktailByIdRep,
    @required this.getCocktailByNameRep,
    @required this.getRandomCocktailRep,
  }) : super(CocktailDetailsEmpty());

  Future<void> getCocktailsById(String id) async {
    emit(CocktailDetailsLoading());
    final cocktails = await getCocktailByIdRep(idRepo.Params(id: id));
    cocktails.fold(
      (left) => emit(CocktailDetailsError(ERROR_MESSAGE)),
      (right) => emit(CocktailDetailsLoaded([right])),
    );
  }

  Future<void> getCocktailsByName(String name) async {
    emit(CocktailDetailsLoading());
    final cocktails = await getCocktailByNameRep(nam.Params(name: name));
    cocktails.fold(
      (left) => emit(CocktailDetailsError(ERROR_MESSAGE)),
      (right) => emit(CocktailDetailsLoaded(right)),
    );
  }

  Future<void> getRandomCocktail() async {
    emit(CocktailDetailsLoading());
    final cocktails = await getRandomCocktailRep(NoParams());
    cocktails.fold(
      (left) => emit(CocktailDetailsError(ERROR_MESSAGE)),
      (right) => emit(CocktailDetailsLoaded([right])),
    );
  }
}
