part of 'favourites_cubit.dart';

abstract class FavouritesState extends Equatable {
  const FavouritesState();

  @override
  List<Object> get props => [];
}

class FavouritesEmpty extends FavouritesState {}

class FavouritesLoading extends FavouritesState {
  const FavouritesLoading();
}

class FavouriteAdded extends FavouritesState {
  final int cocktailId;
  const FavouriteAdded(this.cocktailId);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is FavouriteAdded && o.cocktailId == cocktailId;
  }

  @override
  int get hashCode => cocktailId.hashCode;
}

class FavouriteRemoved extends FavouritesState {
  final int cocktailId;
  const FavouriteRemoved(this.cocktailId);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is FavouriteRemoved && o.cocktailId == cocktailId;
  }

  @override
  int get hashCode => cocktailId.hashCode;
}

class FavouritesError extends FavouritesState {
  final String message;
  const FavouritesError(
    this.message,
  );

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is FavouritesError && o.message == message;
  }

  @override
  int get hashCode => message.hashCode;
}

class FavouritesLoaded extends FavouritesState {
  final List<CocktailDetails> cocktailList;
  const FavouritesLoaded(
    this.cocktailList,
  );

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is FavouritesLoaded && listEquals(o.cocktailList, cocktailList);
  }

  @override
  int get hashCode => cocktailList.hashCode;
}
