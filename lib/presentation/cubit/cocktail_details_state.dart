part of 'cocktail_details_cubit.dart';

abstract class CocktailDetailsState extends Equatable {
  const CocktailDetailsState();

  @override
  List<Object> get props => [];
}

class CocktailDetailsEmpty extends CocktailDetailsState {}

class CocktailDetailsLoading extends CocktailDetailsState {
  const CocktailDetailsLoading();
}

class CocktailDetailsLoaded extends CocktailDetailsState {
  final List<CocktailDetails> cocktailDetailsList;
  CocktailDetailsLoaded(
    this.cocktailDetailsList,
  );

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is CocktailDetailsLoaded &&
        listEquals(o.cocktailDetailsList, cocktailDetailsList);
  }

  @override
  int get hashCode => cocktailDetailsList.hashCode;
}

class CocktailDetailsError extends CocktailDetailsState {
  final String message;
  const CocktailDetailsError(
    this.message,
  );

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is CocktailDetailsError && o.message == message;
  }

  @override
  int get hashCode => message.hashCode;
}
