import 'package:cocktail_application/infrastructure/helpers/database_helper.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:http/http.dart' show Client;
import 'package:injectable/injectable.dart';

@module
abstract class RegisterModule {
  @singleton
  DataConnectionChecker get connectionChecker;

  @singleton
  DatabaseHelper get db;

  @singleton
  Client client() => Client();
}
