import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  static final DatabaseHelper _instance = DatabaseHelper.internal();
  factory DatabaseHelper() => _instance;

  final String tableFavourites = 'favouritesTable';
  final String columnId = 'idDrink';
  final String columnTitle = 'strDrink';
  final String columnTags = 'strTags';
  final String columnCategory = 'strCategory';
  final String columnIBA = 'strIBA';
  final String columnAlcoholic = 'strAlcoholic';
  final String columnGlass = 'strGlass';
  final String columnInstructions = 'strInstructions';
  final String columnDrinkThumb = 'strDrinkThumb';
  final String columnIngredients = 'strIngredients';
  final String columnMeasures = 'strMeasures';

  static Database _db;

  DatabaseHelper.internal();

  Future<Database> get db async {
    if (_db != null) {
      return _db;
    }
    _db = await initDb();

    return _db;
  }

  initDb() async {
    String databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'favourites.db');

    //await deleteDatabase(path); // just for testing

    var db = await openDatabase(path, version: 1, onCreate: _onCreate);
    return db;
  }

  void _onCreate(Database db, int newVersion) async {
    await db.execute('''CREATE TABLE $tableFavourites(
          $columnId TEXT PRIMARY KEY, 
          $columnTitle TEXT, 
          $columnTags TEXT, 
          $columnCategory TEXT, 
          $columnIBA TEXT, 
          $columnAlcoholic TEXT, 
          $columnGlass TEXT,
          $columnInstructions TEXT,
          $columnDrinkThumb TEXT,
          $columnIngredients TEXT,
          $columnMeasures TEXT
          )''');
  }

  /// Accepts a Map of CocktailDetails and returns an {int} of the ID
  Future<int> insertCocktail(Map<String, dynamic> mappedCocktail) async {
    var dbClient = await db;
    var result = await dbClient.insert(
      tableFavourites,
      mappedCocktail,
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return result;
  }

  /// Accepts a CocktailDetails and returns an {int} of the ID of the removed Cocktail
  Future<int> removeCocktail(String cocktailId) async {
    var dbClient = await db;
    var result = dbClient.delete(tableFavourites,
        where: "$columnId = ?", whereArgs: [cocktailId]);
    return result;
  }

  /// Accepts a CocktailDetails and returns an Map of CocktailDetailsModel
  Future<List<Map<String, dynamic>>> getCocktail(String cocktailId) async {
    var dbClient = await db;
    var result = await dbClient.query(tableFavourites,
        where: "$columnId = ?", whereArgs: [cocktailId]);
    return result;
  }

  /// Accepts a List of Maps of CocktailDetails
  Future<List<Map<String, dynamic>>> getCocktails() async {
    var dbClient = await db;
    List<Map<String, dynamic>> result = await dbClient.query(tableFavourites);
    return result;
  }
}
