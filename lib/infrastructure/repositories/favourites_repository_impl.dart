import 'package:cocktail_application/core/errors/exceptions.dart';
import 'package:cocktail_application/domain/entities/cocktail_details.dart';
import 'package:cocktail_application/core/errors/failures.dart';
import 'package:cocktail_application/domain/repositories/favourites_repository.dart';
import 'package:cocktail_application/infrastructure/datasources/favourite_cocktails_local_data_source.dart';
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

@Injectable(as: FavouritesRepository)
class FavouritesRepositoryImpl extends FavouritesRepository {
  final FavouriteCocktailsLocalDataSource favouriteCocktailsLocalDataSource;

  FavouritesRepositoryImpl(this.favouriteCocktailsLocalDataSource);
  @override
  Future<Either<Failure, int>> addCocktailToFavourites(
      CocktailDetails cocktailDetails) async {
    try {
      return right(await favouriteCocktailsLocalDataSource
          .addCocktailToFavourites(cocktailDetails));
    } on EmptyException catch (_) {
      return left(DatabaseFailure());
    }
  }

  @override
  Future<Either<Failure, CocktailDetails>> getFavouriteCocktail(
      String cocktailId) async {
    try {
      return right(await favouriteCocktailsLocalDataSource
          .getFavouriteCocktail(cocktailId));
    } on EmptyException catch (_) {
      return left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, List<CocktailDetails>>> getFavouriteCocktails() async {
    try {
      return right(
          await favouriteCocktailsLocalDataSource.getFavouriteCocktails());
    } on EmptyException catch (_) {
      return left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, int>> removeCocktailFromFavourites(
      String cocktailId) async {
    try {
      return right(await favouriteCocktailsLocalDataSource
          .removeCocktailFromFavourites(cocktailId));
    } on EmptyException catch (_) {
      return left(DatabaseFailure());
    }
  }
}
