import '../../domain/entities/ingredient.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';

import '../../core/errors/exceptions.dart';
import '../../core/errors/failures.dart';
import '../../core/network/network_info.dart';
import '../../domain/entities/cocktail.dart';
import '../../domain/entities/cocktail_details.dart';
import '../../domain/repositories/cocktail_repository.dart';
import '../datasources/cocktail_remote_data_source.dart';

@Injectable(as: CocktailReposity)
class CocktailRepositoryImpl implements CocktailReposity {
  final CocktailRemoteDataSource remoteDataSource;
  final NetworkInfo networkInfo;
  CocktailRepositoryImpl({
    @required this.remoteDataSource,
    @required this.networkInfo,
  });

  @override
  Future<Either<Failure, CocktailDetails>> getCocktailById(
      String cocktailId) async {
    if (await networkInfo.isConnected) {
      try {
        return right(await remoteDataSource.getCocktailById(cocktailId));
      } on ServerException {
        return left(ServerFailure());
      }
    } else {
      return left(OfflineFailure());
    }
  }

  @override
  Future<Either<Failure, List<Cocktail>>> getCocktailsByCategory(
      String category) async {
    if (await networkInfo.isConnected) {
      try {
        return right(await remoteDataSource.getCocktailsByCategory(category));
      } on ServerException {
        return left(ServerFailure());
      }
    } else {
      return left(OfflineFailure());
    }
  }

  @override
  Future<Either<Failure, List<Cocktail>>> getCocktailsByGlass(
      String cocktailGlass) async {
    if (await networkInfo.isConnected) {
      try {
        return right(await remoteDataSource.getCocktailsByGlass(cocktailGlass));
      } on ServerException {
        return left(ServerFailure());
      }
    } else {
      return left(OfflineFailure());
    }
  }

  @override
  Future<Either<Failure, List<Cocktail>>> getCocktailsByIngredient(
      String ingredient) async {
    if (await networkInfo.isConnected) {
      try {
        return right(
            await remoteDataSource.getCocktailsByIngredient(ingredient));
      } on ServerException {
        return left(ServerFailure());
      }
    } else {
      return left(OfflineFailure());
    }
  }

  @override
  Future<Either<Failure, List<CocktailDetails>>> getCocktailsByName(
      String cocktailName) async {
    if (await networkInfo.isConnected) {
      try {
        return right(await remoteDataSource.getCocktailsByName(cocktailName));
      } on ServerException {
        return left(ServerFailure());
      }
    } else {
      return left(OfflineFailure());
    }
  }

  @override
  Future<Either<Failure, List<Ingredient>>> getIngredientsList() async {
    if (await networkInfo.isConnected) {
      try {
        return right(await remoteDataSource.getIngredientsList());
      } on ServerException {
        return left(ServerFailure());
      }
    } else {
      return left(OfflineFailure());
    }
  }

  @override
  Future<Either<Failure, CocktailDetails>> getRandomCocktail() async {
    if (await networkInfo.isConnected) {
      try {
        return right(await remoteDataSource.getRandomCocktail());
      } on ServerException {
        return left(ServerFailure());
      }
    } else {
      return left(OfflineFailure());
    }
  }
}
