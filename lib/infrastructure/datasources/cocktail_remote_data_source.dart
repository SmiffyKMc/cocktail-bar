import '../../domain/entities/ingredient.dart';

import '../models/cocktail_details_model.dart';
import '../models/cocktail_model.dart';

abstract class CocktailRemoteDataSource {
  // * Returns List of Cocktails
  /// Calls the https://www.thecocktaildb.com/api/json/v1/1/filter.php?c={category} endpoint.
  ///
  /// Throws a [ServerException] for all error codes.
  Future<List<CocktailModel>> getCocktailsByCategory(String category);

  /// Calls the https://www.thecocktaildb.com/api/json/v1/1/filter.php?g={cocktailGlass} endpoint.
  ///
  /// Throws a [ServerException] for all error codes.
  Future<List<CocktailModel>> getCocktailsByGlass(String cocktailGlass);

  /// Calls the https://www.thecocktaildb.com/api/json/v1/1/filter.php?i={ingredient} endpoint.
  ///
  /// Throws a [ServerException] for all error codes.
  Future<List<CocktailModel>> getCocktailsByIngredient(String ingredient);

  // * Returns CocktailDetails
  /// Calls the https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i={cocktailId} endpoint.
  ///
  /// Throws a [ServerException] for all error codes.
  Future<CocktailDetailsModel> getCocktailById(String cocktailId);

  /// Calls the https://www.thecocktaildb.com/api/json/v1/1/random.php endpoint.
  ///
  /// Throws a [ServerException] for all error codes.
  Future<CocktailDetailsModel> getRandomCocktail();

  // * Returns List of CocktailDetails
  /// Calls the https://www.thecocktaildb.com/api/json/v1/1/search.php?s={name} endpoint.
  ///
  /// Throws a [ServerException] for all error codes.
  Future<List<CocktailDetailsModel>> getCocktailsByName(String cocktailName);

  // * Returns List of Ingredients
  /// Calls the https://www.thecocktaildb.com/api/json/v1/1/list.php?i=list endpoint.
  ///
  /// Throws a [ServerException] for all error codes.
  Future<List<Ingredient>> getIngredientsList();
}
