import 'package:cocktail_application/core/errors/exceptions.dart';
import 'package:cocktail_application/infrastructure/datasources/favourite_cocktails_local_data_source.dart';
import 'package:cocktail_application/infrastructure/helpers/database_helper.dart';
import 'package:cocktail_application/infrastructure/models/cocktail_details_model.dart';
import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';

@Injectable(as: FavouriteCocktailsLocalDataSource)
class FavouriteCocktailsLocalDataSourceImpl
    implements FavouriteCocktailsLocalDataSource {
  final DatabaseHelper databaseHelper;

  FavouriteCocktailsLocalDataSourceImpl({@required this.databaseHelper});
  @override
  Future<int> addCocktailToFavourites(
      CocktailDetailsModel cocktailDetailsModel) {
    return databaseHelper.insertCocktail(cocktailDetailsModel.toJson());
  }

  @override
  Future<CocktailDetailsModel> getFavouriteCocktail(String cocktailId) async {
    final favouriteCocktail = await databaseHelper.getCocktail(cocktailId);
    try {
      if (favouriteCocktail.length > 0) {
        return CocktailDetailsModel.fromJson(favouriteCocktail.first);
      } else {
        throw EmptyException();
      }
    } on StateError catch (_) {
      throw DatabaseException();
    }
  }

  @override
  Future<List<CocktailDetailsModel>> getFavouriteCocktails() async {
    final List<CocktailDetailsModel> favouritesList = [];
    try {
      final favouriteCocktails = await databaseHelper.getCocktails();
      if (favouriteCocktails.isEmpty) throw EmptyException();
      favouriteCocktails.forEach((favouriteCocktail) {
        favouritesList.add(CocktailDetailsModel.fromJson(favouriteCocktail));
      });
      return favouritesList;
    } on StateError catch (_) {
      throw EmptyException();
    }
  }

  @override
  Future<int> removeCocktailFromFavourites(String cocktailId) async {
    try {
      final favouriteCocktailRemoved =
          await databaseHelper.removeCocktail(cocktailId);
      return favouriteCocktailRemoved;
    } on StateError catch (_) {
      throw DatabaseException();
    } on Exception catch (_) {
      throw DatabaseException();
    }
  }
}
