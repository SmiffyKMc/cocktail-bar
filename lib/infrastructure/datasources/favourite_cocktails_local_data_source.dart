import 'package:cocktail_application/infrastructure/models/cocktail_details_model.dart';

abstract class FavouriteCocktailsLocalDataSource {
  // Calls the local database call for getFavouriteCocktails.
  ///
  /// Throws a [EmptyException] if no favourites exist.
  Future<List<CocktailDetailsModel>> getFavouriteCocktails();
  // Calls the local database call for getFavouriteCocktail.
  ///
  /// Throws a [EmptyException] if no favourites exist.
  Future<CocktailDetailsModel> getFavouriteCocktail(String cocktailId);
  // Calls the local database call for removeCocktailFromFavourites.
  ///
  /// returns false if removing cocktail failed.
  Future<int> removeCocktailFromFavourites(String cocktailId);
  // Calls the local database call for addCocktailToFavourites.
  ///
  /// returns false if adding cocktail failed.
  Future<int> addCocktailToFavourites(CocktailDetailsModel cocktailDetails);
}
