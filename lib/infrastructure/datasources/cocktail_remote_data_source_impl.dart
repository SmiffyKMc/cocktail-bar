import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:injectable/injectable.dart';

import '../../core/errors/exceptions.dart';
import '../../domain/entities/ingredient.dart';
import '../models/cocktail_details_model.dart';
import '../models/cocktail_details_list_model.dart';
import '../models/cocktail_model.dart';
import '../models/cocktails_list_model.dart';
import '../models/ingredient_list_model.dart';
import 'cocktail_remote_data_source.dart';

@Injectable(as: CocktailRemoteDataSource)
class CocktailRemoteDataSourceImpl implements CocktailRemoteDataSource {
  final http.Client client;
  CocktailRemoteDataSourceImpl({
    @required this.client,
  });
  @override
  Future<CocktailDetailsModel> getCocktailById(String cocktailId) async {
    final response = await client.get(
      'https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=$cocktailId',
      headers: {'Content-Type': 'application/json'},
    );
    if (response.statusCode == 200) {
      try {
        return CocktailDetailsListModel.fromJson(json.decode(response.body))
            .cocktailDetailsList
            .first;
      } on StateError {
        throw ServerException();
      }
    } else {
      throw ServerException();
    }
  }

  @override
  Future<List<CocktailModel>> getCocktailsByCategory(String category) async {
    final response = await client.get(
        'https://www.thecocktaildb.com/api/json/v1/1/filter.php?c=$category',
        headers: {'Content-Type': 'application/json'});
    if (response.statusCode == 200) {
      return CocktailsListModel.fromJson(json.decode(response.body))
          .cocktailsList;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<List<CocktailModel>> getCocktailsByGlass(String cocktailGlass) async {
    final response = await client.get(
        'https://www.thecocktaildb.com/api/json/v1/1/filter.php?g=$cocktailGlass',
        headers: {'Content-Type': 'application/json'});
    if (response.statusCode == 200) {
      return CocktailsListModel.fromJson(json.decode(response.body))
          .cocktailsList;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<List<CocktailModel>> getCocktailsByIngredient(
      String ingredient) async {
    final response = await client.get(
        'https://www.thecocktaildb.com/api/json/v1/1/filter.php?i=$ingredient',
        headers: {'Content-Type': 'application/json'});
    if (response.statusCode == 200) {
      return CocktailsListModel.fromJson(json.decode(response.body))
          .cocktailsList;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<List<CocktailDetailsModel>> getCocktailsByName(
      String cocktailName) async {
    final response = await client.get(
        'https://www.thecocktaildb.com/api/json/v1/1/search.php?s=$cocktailName',
        headers: {'Content-Type': 'application/json'});
    if (response.statusCode == 200) {
      return CocktailDetailsListModel.fromJson(json.decode(response.body))
          .cocktailDetailsList;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<List<Ingredient>> getIngredientsList() async {
    final response = await client.get(
        'https://www.thecocktaildb.com/api/json/v1/1/list.php?i=list',
        headers: {'Content-Type': 'application/json'});
    if (response.statusCode == 200) {
      return IngredientListModel.fromJson(json.decode(response.body))
          .ingredientList;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<CocktailDetailsModel> getRandomCocktail() async {
    final response = await client.get(
      'https://www.thecocktaildb.com/api/json/v1/1/random.php',
      headers: {'Content-Type': 'application/json'},
    );
    if (response.statusCode == 200) {
      try {
        return CocktailDetailsListModel.fromJson(json.decode(response.body))
            .cocktailDetailsList
            .first;
      } on StateError {
        throw ServerException();
      }
    } else {
      throw ServerException();
    }
  }
}
