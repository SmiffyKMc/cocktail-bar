import 'package:flutter/foundation.dart';

import '../../domain/entities/cocktails_list.dart';
import 'cocktail_model.dart';

class CocktailsListModel extends CocktailsList {
  CocktailsListModel({@required List<CocktailModel> cocktailsList})
      : super(cocktailsList: cocktailsList);

  Map<String, dynamic> toJson() {
    final cocktailsListMapped = cocktailsList
        .map((cocktail) => {
              "idDrink": cocktail.idDrink,
              "strDrink": cocktail.strDrink,
              "strDrinkThumb": cocktail.strDrinkThumb
            })
        .toList();
    return {"drinks": cocktailsListMapped};
  }

  //? The data comes into the application as a map where drinks is the owner.
  factory CocktailsListModel.fromJson(Map<String, dynamic> json) {
    Iterable cocktailsIterable = json['drinks'];
    List<CocktailModel> cocktails = cocktailsIterable
        .map((model) => CocktailModel.fromJson(model))
        .toList();
    return CocktailsListModel(cocktailsList: cocktails);
  }
}
