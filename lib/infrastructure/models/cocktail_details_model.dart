import 'dart:convert';

import 'package:flutter/foundation.dart';

import '../../domain/entities/cocktail_details.dart';

class CocktailDetailsModel extends CocktailDetails {
  CocktailDetailsModel({
    @required String idDrink,
    @required String strDrink,
    @required String strTags,
    @required String strCategory,
    @required String strIBA,
    @required String strAlcoholic,
    @required String strGlass,
    @required String strInstructions,
    @required String strDrinkThumb,
    @required List<String> strIngredients,
    @required List<String> strMeasures,
  }) : super(
            idDrink: idDrink,
            strDrink: strDrink,
            strTags: strTags,
            strCategory: strCategory,
            strIBA: strIBA,
            strAlcoholic: strAlcoholic,
            strGlass: strGlass,
            strInstructions: strInstructions,
            strDrinkThumb: strDrinkThumb,
            strIngredients: strIngredients,
            strMeasures: strMeasures);

  Map<String, dynamic> toJson() {
    return {
      "idDrink": idDrink,
      "strDrink": strDrink,
      "strTags": strTags,
      "strCategory": strCategory,
      "strIBA": strIBA,
      "strAlcoholic": strAlcoholic,
      "strGlass": strGlass,
      "strInstructions": strInstructions,
      "strDrinkThumb": strDrinkThumb,
      "strIngredients": json.encode(strIngredients),
      "strMeasures": json.encode(strMeasures)
    };
  }

  //? Simple map, but creating a list of ingredients and measures.
  factory CocktailDetailsModel.fromJson(Map<String, dynamic> json) {
    return CocktailDetailsModel(
      idDrink: json['idDrink'],
      strDrink: json['strDrink'],
      strTags: json['strTags'],
      strCategory: json['strCategory'],
      strIBA: json['strIBA'],
      strAlcoholic: json['strAlcoholic'],
      strGlass: json['strGlass'],
      strInstructions: json['strInstructions'],
      strDrinkThumb: json['strDrinkThumb'],
      strIngredients: _getIngredientsList(json),
      strMeasures: _getMeasuresList(json),
    );
  }
}

List<String> _getIngredientsList(Map<String, dynamic> jsonData) {
  return _getListFromKeys(jsonData, 'strIngredient');
}

List<String> _getMeasuresList(Map<String, dynamic> jsonData) {
  return _getListFromKeys(jsonData, 'strMeasure');
}

List<String> _getListFromKeys(Map<String, dynamic> jsonData, String key) {
  List<String> listToReturn = [];
  if (jsonData.containsKey('${key}1')) {
    for (var i = 1; i < 16; i++) {
      listToReturn.add(jsonData['$key$i']);
    }
  } else {
    listToReturn = List<String>.from(json.decode(jsonData['${key}s']));
  }
  return listToReturn;
}
