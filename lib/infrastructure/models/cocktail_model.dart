import 'package:flutter/foundation.dart';

import '../../domain/entities/cocktail.dart';

class CocktailModel extends Cocktail {
  CocktailModel(
      {@required String idDrink,
      @required String strDrink,
      @required String strDrinkThumb})
      : super(
          idDrink: idDrink,
          strDrink: strDrink,
          strDrinkThumb: strDrinkThumb,
        );

  Map<String, dynamic> toJson() {
    return {
      "idDrink": idDrink,
      "strDrink": strDrink,
      "strDrinkThumb": strDrinkThumb
    };
  }

  factory CocktailModel.fromJson(Map<String, dynamic> json) {
    return CocktailModel(
        idDrink: json['idDrink'],
        strDrink: json['strDrink'],
        strDrinkThumb: json['strDrinkThumb']);
  }
}
