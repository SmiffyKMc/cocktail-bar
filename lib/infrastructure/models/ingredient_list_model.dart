import 'package:flutter/foundation.dart';

import '../../domain/entities/ingredient.dart';
import '../../domain/entities/ingredient_list.dart';
import 'ingredient_model.dart';

class IngredientListModel extends IngredientList {
  IngredientListModel({@required List<Ingredient> ingredientList})
      : super(ingredientList: ingredientList);

  Map<String, dynamic> toJson() {
    final ingredientListMapped = ingredientList
        .map((ingredient) => {"strIngredient1": ingredient.strIngredient1})
        .toList();
    return {"drinks": ingredientListMapped};
  }

  //? The data comes into the application as a map where drinks is the owner.
  factory IngredientListModel.fromJson(Map<String, dynamic> json) {
    Iterable ingredientIterable = json['drinks'];
    List<Ingredient> ingredients = ingredientIterable
        .map((model) => IngredientModel.fromJson(model))
        .toList();
    return IngredientListModel(ingredientList: ingredients);
  }
}
