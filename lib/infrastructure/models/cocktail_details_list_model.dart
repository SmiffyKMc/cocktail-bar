import 'package:flutter/foundation.dart';

import '../../domain/entities/cocktail_details_list.dart';
import 'cocktail_details_model.dart';

class CocktailDetailsListModel extends CocktailDetailsList {
  CocktailDetailsListModel(
      {@required List<CocktailDetailsModel> cocktailDetailsList})
      : super(cocktailDetailsList: cocktailDetailsList);

  Map<String, dynamic> toJson() {
    final cocktailDetailsListMapped = cocktailDetailsList
        .map((cocktail) => {
              "idDrink": cocktail.idDrink,
              "strDrink": cocktail.strDrink,
              "strTags": cocktail.strTags,
              "strCategory": cocktail.strCategory,
              "strIBA": cocktail.strIBA,
              "strAlcoholic": cocktail.strAlcoholic,
              "strGlass": cocktail.strGlass,
              "strInstructions": cocktail.strInstructions,
              "strDrinkThumb": cocktail.strDrinkThumb,
              "strIngredients": cocktail.strIngredients,
              "strMeasures": cocktail.strMeasures
            })
        .toList();
    return {"drinks": cocktailDetailsListMapped};
  }

  //? The data comes into the application as a map where drinks is the owner.
  factory CocktailDetailsListModel.fromJson(Map<String, dynamic> json) {
    Iterable cocktailsIterable = json['drinks'];
    List<CocktailDetailsModel> cocktails = cocktailsIterable
        .map((model) => CocktailDetailsModel.fromJson(model))
        .toList();
    return CocktailDetailsListModel(cocktailDetailsList: cocktails);
  }
}
