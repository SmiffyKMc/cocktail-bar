import '../../domain/entities/ingredient.dart';
import 'package:flutter/foundation.dart';

class IngredientModel extends Ingredient {
  IngredientModel({@required String strIngredient1})
      : super(strIngredient1: strIngredient1);

  Map<String, dynamic> toJson() {
    return {
      "strIngredient1": strIngredient1,
    };
  }

  factory IngredientModel.fromJson(Map<String, dynamic> json) {
    return IngredientModel(strIngredient1: json['strIngredient1']);
  }
}
