import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';

import '../../../core/errors/failures.dart';
import '../../../core/usecases/usecase.dart';
import '../../entities/cocktail_details.dart';
import '../../repositories/cocktail_repository.dart';

@injectable
class GetCocktailsByName extends UseCase<List<CocktailDetails>, Params> {
  final CocktailReposity cocktailReposity;
  GetCocktailsByName(
    this.cocktailReposity,
  );
  @override
  Future<Either<Failure, List<CocktailDetails>>> call(Params params) async {
    return await cocktailReposity.getCocktailsByName(params.name);
  }
}

class Params extends Equatable {
  final String name;
  Params({
    @required this.name,
  });

  @override
  List<Object> get props => [name];
}
