import '../../entities/ingredient.dart';
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

import '../../../core/errors/failures.dart';
import '../../../core/usecases/usecase.dart';
import '../../repositories/cocktail_repository.dart';

@injectable
class GetIngredientsList extends UseCase<List<Ingredient>, NoParams> {
  CocktailReposity cocktailReposity;
  GetIngredientsList(
    this.cocktailReposity,
  );

  @override
  Future<Either<Failure, List<Ingredient>>> call(NoParams params) async {
    return await cocktailReposity.getIngredientsList();
  }
}
