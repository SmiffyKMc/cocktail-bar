import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';

import '../../../core/errors/failures.dart';
import '../../../core/usecases/usecase.dart';
import '../../entities/cocktail.dart';
import '../../repositories/cocktail_repository.dart';

@injectable
class GetCocktailsByGlass extends UseCase<List<Cocktail>, Params> {
  final CocktailReposity cocktailReposity;
  GetCocktailsByGlass(
    this.cocktailReposity,
  );

  @override
  Future<Either<Failure, List<Cocktail>>> call(Params params) async {
    return await cocktailReposity.getCocktailsByGlass(params.glass);
  }
}

class Params extends Equatable {
  final String glass;
  Params({
    @required this.glass,
  });

  @override
  List<Object> get props => [glass];
}
