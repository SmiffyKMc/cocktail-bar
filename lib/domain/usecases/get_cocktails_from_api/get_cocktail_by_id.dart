import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';

import '../../../core/errors/failures.dart';
import '../../../core/usecases/usecase.dart';
import '../../entities/cocktail_details.dart';
import '../../repositories/cocktail_repository.dart';

@injectable
class GetCocktailById extends UseCase<CocktailDetails, Params> {
  final CocktailReposity cocktailReposity;
  GetCocktailById(
    this.cocktailReposity,
  );

  @override
  Future<Either<Failure, CocktailDetails>> call(Params params) async {
    return await cocktailReposity.getCocktailById(params.id);
  }
}

class Params extends Equatable {
  final String id;
  Params({
    @required this.id,
  });

  @override
  List<Object> get props => [id];
}
