import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';

import '../../../core/errors/failures.dart';
import '../../../core/usecases/usecase.dart';
import '../../entities/cocktail.dart';
import '../../repositories/cocktail_repository.dart';

@injectable
class GetCocktailsByCategory extends UseCase<List<Cocktail>, Params> {
  final CocktailReposity cocktailReposity;
  GetCocktailsByCategory(
    this.cocktailReposity,
  );
  @override
  Future<Either<Failure, List<Cocktail>>> call(Params params) async {
    return await cocktailReposity.getCocktailsByCategory(params.category);
  }
}

class Params extends Equatable {
  final String category;
  Params({
    @required this.category,
  });

  @override
  List<Object> get props => [category];
}
