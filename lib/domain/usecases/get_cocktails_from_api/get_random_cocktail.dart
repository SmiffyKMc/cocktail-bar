import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

import '../../../core/errors/failures.dart';
import '../../../core/usecases/usecase.dart';
import '../../entities/cocktail_details.dart';
import '../../repositories/cocktail_repository.dart';

@injectable
class GetRandomCocktail extends UseCase<CocktailDetails, NoParams> {
  final CocktailReposity cocktailReposity;
  GetRandomCocktail(
    this.cocktailReposity,
  );

  @override
  Future<Either<Failure, CocktailDetails>> call(NoParams noParams) async {
    return await cocktailReposity.getRandomCocktail();
  }
}
