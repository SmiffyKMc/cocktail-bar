import 'package:cocktail_application/core/errors/failures.dart';
import 'package:cocktail_application/core/usecases/usecase.dart';
import 'package:cocktail_application/domain/entities/cocktail_details.dart';
import 'package:cocktail_application/domain/repositories/favourites_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

@injectable
class GetFavouriteCocktails extends UseCase<List<CocktailDetails>, NoParams> {
  final FavouritesRepository favouritesRepository;

  GetFavouriteCocktails(this.favouritesRepository);
  @override
  Future<Either<Failure, List<CocktailDetails>>> call(NoParams noParams) async {
    return await favouritesRepository.getFavouriteCocktails();
  }
}
