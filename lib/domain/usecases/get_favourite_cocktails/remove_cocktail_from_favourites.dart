import 'package:cocktail_application/core/errors/failures.dart';
import 'package:cocktail_application/core/usecases/usecase.dart';
import 'package:cocktail_application/domain/repositories/favourites_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';

@injectable
class RemoveCocktailFromFavourites extends UseCase<int, Params> {
  final FavouritesRepository favouritesRepository;

  RemoveCocktailFromFavourites(this.favouritesRepository);
  @override
  Future<Either<Failure, int>> call(params) async {
    return await favouritesRepository
        .removeCocktailFromFavourites(params.cocktailId);
  }
}

class Params extends Equatable {
  final String cocktailId;
  Params({
    @required this.cocktailId,
  });

  @override
  List<Object> get props => [cocktailId];
}
