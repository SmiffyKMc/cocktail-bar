import 'package:cocktail_application/core/errors/failures.dart';
import 'package:cocktail_application/core/usecases/usecase.dart';
import 'package:cocktail_application/domain/entities/cocktail_details.dart';
import 'package:cocktail_application/domain/repositories/favourites_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';

@injectable
class AddCocktailToFavourites extends UseCase<int, Params> {
  final FavouritesRepository favouritesRepository;

  AddCocktailToFavourites(this.favouritesRepository);
  @override
  Future<Either<Failure, int>> call(params) async {
    return await favouritesRepository
        .addCocktailToFavourites(params.cocktailDetails);
  }
}

class Params extends Equatable {
  final CocktailDetails cocktailDetails;
  Params({
    @required this.cocktailDetails,
  });

  @override
  List<Object> get props => [cocktailDetails];
}
