import 'package:cocktail_application/core/errors/failures.dart';
import 'package:cocktail_application/core/usecases/usecase.dart';
import 'package:cocktail_application/domain/entities/cocktail_details.dart';
import 'package:cocktail_application/domain/repositories/favourites_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';

@injectable
class GetFavouriteCocktail extends UseCase<CocktailDetails, Params> {
  final FavouritesRepository favouritesRepository;

  GetFavouriteCocktail(this.favouritesRepository);
  @override
  Future<Either<Failure, CocktailDetails>> call(params) async {
    return await favouritesRepository.getFavouriteCocktail(params.cocktailId);
  }
}

class Params extends Equatable {
  final String cocktailId;
  Params({
    @required this.cocktailId,
  });

  @override
  List<Object> get props => [cocktailId];
}
