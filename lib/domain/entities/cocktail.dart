import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

class Cocktail extends Equatable {
  final String idDrink;
  final String strDrink;
  final String strDrinkThumb;

  Cocktail({
    @required this.idDrink,
    @required this.strDrink,
    @required this.strDrinkThumb,
  });

  @override
  List<Object> get props => [
        idDrink,
        strDrink,
        strDrinkThumb,
      ];

  @override
  bool get stringify => true;
}
