import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

class CocktailDetails extends Equatable {
  final String idDrink;
  final String strDrink;
  final String strTags;
  final String strCategory;
  final String strIBA;
  final String strAlcoholic;
  final String strGlass;
  final String strInstructions;
  final String strDrinkThumb;
  final List<String> strIngredients;
  final List<String> strMeasures;

  CocktailDetails({
    @required this.idDrink,
    @required this.strDrink,
    @required this.strTags,
    @required this.strCategory,
    @required this.strIBA,
    @required this.strAlcoholic,
    @required this.strGlass,
    @required this.strInstructions,
    @required this.strDrinkThumb,
    @required this.strIngredients,
    @required this.strMeasures,
  });

  @override
  List<Object> get props => [
        idDrink,
        strDrink,
        strTags,
        strCategory,
        strIBA,
        strAlcoholic,
        strGlass,
        strInstructions,
        strDrinkThumb,
        strIngredients,
        strMeasures,
      ];

  @override
  bool get stringify => true;
}
