import 'cocktail_details.dart';

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

class CocktailDetailsList extends Equatable {
  final List<CocktailDetails> cocktailDetailsList;
  CocktailDetailsList({
    @required this.cocktailDetailsList,
  });
  @override
  List<Object> get props => [];
}
