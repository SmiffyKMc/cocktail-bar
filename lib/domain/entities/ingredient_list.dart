import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

import 'ingredient.dart';

class IngredientList extends Equatable {
  final List<Ingredient> ingredientList;
  IngredientList({
    @required this.ingredientList,
  });
  @override
  List<Object> get props => [];
}
