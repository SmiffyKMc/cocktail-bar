import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

import 'cocktail.dart';

class CocktailsList extends Equatable {
  final List<Cocktail> cocktailsList;
  CocktailsList({
    @required this.cocktailsList,
  });
  @override
  List<Object> get props => [];
}
