import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

class Ingredient extends Equatable {
  final String strIngredient1;
  Ingredient({
    @required this.strIngredient1,
  });
  @override
  List<Object> get props => [strIngredient1];
}
