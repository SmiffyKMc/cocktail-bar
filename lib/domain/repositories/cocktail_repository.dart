import 'package:dartz/dartz.dart';

import '../../core/errors/failures.dart';
import '../entities/cocktail.dart';
import '../entities/cocktail_details.dart';
import '../entities/ingredient.dart';

abstract class CocktailReposity {
  // * Returns List of Cocktails
  Future<Either<Failure, List<Cocktail>>> getCocktailsByCategory(
      String category);
  Future<Either<Failure, List<Cocktail>>> getCocktailsByGlass(
      String cocktailGlass);
  Future<Either<Failure, List<Cocktail>>> getCocktailsByIngredient(
      String ingredient);
  // * Returns CocktailDetails
  Future<Either<Failure, CocktailDetails>> getCocktailById(String cocktailId);
  Future<Either<Failure, CocktailDetails>> getRandomCocktail();
  // * Returns List of CocktailDetails
  Future<Either<Failure, List<CocktailDetails>>> getCocktailsByName(
      String cocktailName);
  // * Returns List of Ingredients
  Future<Either<Failure, List<Ingredient>>> getIngredientsList();
}
