import 'package:cocktail_application/core/errors/failures.dart';
import 'package:cocktail_application/domain/entities/cocktail_details.dart';
import 'package:dartz/dartz.dart';

abstract class FavouritesRepository {
  Future<Either<Failure, List<CocktailDetails>>> getFavouriteCocktails();
  Future<Either<Failure, CocktailDetails>> getFavouriteCocktail(
      String cocktailId);
  Future<Either<Failure, int>> removeCocktailFromFavourites(String cocktailId);
  Future<Either<Failure, int>> addCocktailToFavourites(
      CocktailDetails cocktailDetails);
}
