// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:http/http.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';

import 'domain/usecases/get_favourite_cocktails/add_cocktail_to_favourites.dart';
import 'presentation/cubit/cocktail_details_cubit.dart';
import 'infrastructure/datasources/cocktail_remote_data_source.dart';
import 'infrastructure/datasources/cocktail_remote_data_source_impl.dart';
import 'infrastructure/repositories/cocktail_repository_impl.dart';
import 'domain/repositories/cocktail_repository.dart';
import 'presentation/cubit/cocktails_cubit.dart';
import 'infrastructure/helpers/database_helper.dart';
import 'infrastructure/datasources/favourite_cocktails_local_data_source.dart';
import 'infrastructure/datasources/favourite_cocktails_local_data_source_impl.dart';
import 'presentation/cubit/favourites_cubit.dart';
import 'domain/repositories/favourites_repository.dart';
import 'infrastructure/repositories/favourites_repository_impl.dart';
import 'domain/usecases/get_cocktails_from_api/get_cocktail_by_id.dart';
import 'domain/usecases/get_cocktails_from_api/get_cocktails_by_category.dart';
import 'domain/usecases/get_cocktails_from_api/get_cocktails_by_glass.dart';
import 'domain/usecases/get_cocktails_from_api/get_cocktails_by_ingredient.dart';
import 'domain/usecases/get_cocktails_from_api/get_cocktails_by_name.dart';
import 'domain/usecases/get_favourite_cocktails/get_favourtie_cocktail.dart';
import 'domain/usecases/get_favourite_cocktails/get_favourite_cocktails.dart';
import 'domain/usecases/get_cocktails_from_api/get_ingredients_list.dart';
import 'domain/usecases/get_cocktails_from_api/get_random_cocktail.dart';
import 'core/network/network_info.dart';
import 'register_module.dart';
import 'domain/usecases/get_favourite_cocktails/remove_cocktail_from_favourites.dart';

/// adds generated dependencies
/// to the provided [GetIt] instance

GetIt $initGetIt(
  GetIt get, {
  String environment,
  EnvironmentFilter environmentFilter,
}) {
  final gh = GetItHelper(get, environment, environmentFilter);
  final registerModule = _$RegisterModule();
  gh.factory<CocktailRemoteDataSource>(
      () => CocktailRemoteDataSourceImpl(client: get<Client>()));
  gh.factory<FavouriteCocktailsLocalDataSource>(() =>
      FavouriteCocktailsLocalDataSourceImpl(
          databaseHelper: get<DatabaseHelper>()));
  gh.factory<FavouritesRepository>(
      () => FavouritesRepositoryImpl(get<FavouriteCocktailsLocalDataSource>()));
  gh.factory<GetFavouriteCocktail>(
      () => GetFavouriteCocktail(get<FavouritesRepository>()));
  gh.factory<GetFavouriteCocktails>(
      () => GetFavouriteCocktails(get<FavouritesRepository>()));
  gh.factory<NetworkInfo>(() => NetworkInfoImpl(get<DataConnectionChecker>()));
  gh.factory<RemoveCocktailFromFavourites>(
      () => RemoveCocktailFromFavourites(get<FavouritesRepository>()));
  gh.factory<AddCocktailToFavourites>(
      () => AddCocktailToFavourites(get<FavouritesRepository>()));
  gh.factory<CocktailReposity>(() => CocktailRepositoryImpl(
      remoteDataSource: get<CocktailRemoteDataSource>(),
      networkInfo: get<NetworkInfo>()));
  gh.factory<FavouritesCubit>(() => FavouritesCubit(
        getFavouriteCocktailsRep: get<GetFavouriteCocktails>(),
        getFavouriteCocktailRep: get<GetFavouriteCocktail>(),
        addCocktailToFavouritesRep: get<AddCocktailToFavourites>(),
        removeCocktailFromFavouritesRep: get<RemoveCocktailFromFavourites>(),
      ));
  gh.factory<GetCocktailById>(() => GetCocktailById(get<CocktailReposity>()));
  gh.factory<GetCocktailsByCategory>(
      () => GetCocktailsByCategory(get<CocktailReposity>()));
  gh.factory<GetCocktailsByGlass>(
      () => GetCocktailsByGlass(get<CocktailReposity>()));
  gh.factory<GetCocktailsByIngredient>(
      () => GetCocktailsByIngredient(get<CocktailReposity>()));
  gh.factory<GetCocktailsByName>(
      () => GetCocktailsByName(get<CocktailReposity>()));
  gh.factory<GetIngredientsList>(
      () => GetIngredientsList(get<CocktailReposity>()));
  gh.factory<GetRandomCocktail>(
      () => GetRandomCocktail(get<CocktailReposity>()));
  gh.factory<CocktailDetailsCubit>(() => CocktailDetailsCubit(
        getCocktailByIdRep: get<GetCocktailById>(),
        getCocktailByNameRep: get<GetCocktailsByName>(),
        getRandomCocktailRep: get<GetRandomCocktail>(),
      ));
  gh.factory<CocktailsCubit>(() => CocktailsCubit(
        getCocktailsByCategoryRep: get<GetCocktailsByCategory>(),
        getCocktailsByGlassRep: get<GetCocktailsByGlass>(),
        getCocktailsByIngredientRep: get<GetCocktailsByIngredient>(),
      ));

  // Eager singletons must be registered in the right order
  gh.singleton<Client>(registerModule.client());
  gh.singleton<DataConnectionChecker>(registerModule.connectionChecker);
  gh.singleton<DatabaseHelper>(registerModule.db);
  return get;
}

class _$RegisterModule extends RegisterModule {
  @override
  DataConnectionChecker get connectionChecker => DataConnectionChecker();
  @override
  DatabaseHelper get db => DatabaseHelper();
}
